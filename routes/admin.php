<?php 
use Illuminate\Http\Request;

Route::group(['middleware' => 'cors'], function () {
    Route::get('/', ['uses' => 'UsuarioController@login']);
    Route::get('/loginSaveByToken', ['uses' => 'UsuarioController@saveUserByToken']);

    Route::group(['middleware' => 'auth:admin'], function () {
        Route::get('/dashboard', ['uses' => 'DashboardController@index']);

        Route::resource('/usuarios', 'UsuarioController');
        Route::resource('/locais', 'LocalsController');
        Route::post('/logout', ['uses' => 'UsuarioController@logout']);
    });
    
});