<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'cors'], function () {
    Route::resource('config', 'ConfiguracaosController');
    Route::get('/check', ['uses' => 'UsuarioController@checkToken']);
    Route::post('/cadastro', ['uses' => 'UsuarioController@store']);

    Route::group(['middleware' => 'auth:api'], function () {        
        Route::resource('users', 'UsuarioController');
        Route::get('/user/getUserInfo', ['uses' => 'UsuarioController@getUser']);
        
        Route::post('/logout', ['uses' => 'UsuarioController@logout']);

        Route::post('/mail/send/{layout}', ['uses' => 'MailController@send']);
    });
});
