window.onload = function() {
    $('#pessoas-lista').DataTable({
        language: { url: "dataTable.portugues.txt"},
        responsive: true,
        serverSide: true,
        processing: true,
        ajax: { 
            url: params.url, 
            type:'POST', 
            data: {
                "dbcolumns": params.dbcolumns,
                "dbjoins":   params.dbjoins,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            } 
        },
        columns: params.columns
    });
};
    