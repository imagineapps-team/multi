function actions(){};

actions.prototype.get = function(controller, token = false, params = false){
    const headers = {
        "X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr('content'),
        'Accept': 'application/json',
        'Content-type': 'application/json'
    };

    if(token) headers.Authorization = 'Bearer '+token;

    var url = host+controller; //falta params
    return new Promise((resolve, reject) => {
        $.ajax({
            method: 'GET',
            type:'json',
            headers: headers,
            url: url 
        }).done(function(success){
            resolve(success);
        }).fail(function(error){
            reject(error);
        });
    }) 
    
}

actions.prototype.delete = function(controller, id){
    return new Promise((resolve, reject) => {
        $.ajax({
            method:'DELETE',
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr('content')},
            url: host+controller+'/'+id
        }).done(function(data){
            resolve({success:true, message: 'Registro apagado!'});
        }).fail(function(data){
            reject({success:false, message:JSON.parse(data.responseText)});
        });
    })
};

actions.prototype.insert = function(controller, data){
    return new Promise((resolve, reject) => {
        $.ajax({
            method:'POST',
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr('content'),"Accept":"application/json"},
            url: host+controller,
            data: data
        }).done(function(r){
            console.log(r);
            resolve({success:true, data:r.data, message: r.message});
        }).fail(function(data){
            reject({success:false, retorno:JSON.parse(data.responseText)});
        });
    })
};

actions.prototype.update = function(controller, data, id){
    return new Promise((resolve, reject) => {
        $.ajax({
            method:'PUT',
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr('content'), "Accept":"application/json"},
            url: host+controller+'/'+id,
            data: data
        }).done(function(r){
            console.log(r);
            resolve({success:true, data:r.data, message: r.message});
        }).fail(function(data){
            reject({success:false, retorno:JSON.parse(data.responseText)});
        });
    })
};

actions.prototype.login = function(controller, data){
    return new Promise((resolve, reject) => {
        $.ajax({
            method:'POST',
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr('content')},
            url: host+controller,
            data: data
        }).done(function(data){
            resolve({success:true, message: data.success, token:data.access_token});
        }).fail(function(data){
            reject({success:false, retorno:JSON.parse(data.responseText)});
        });
    })
}

actions.prototype.searchInField = function(force, controller, action,  term){
    return new Promise((resolve, reject) => {
        $.get(host+controller+'/'+action+'/' + term, function(data) {
            resolve(data.results);
        });
    });
};

actions.prototype.specified = function(controller, action, data){
    
    return new Promise((resolve, reject) => {
        $.ajax({
            type:'PUT',
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr('content')},
            url: host+controller+'/'+action,
            data: data
        }).done(function(data){
            resolve({success:true, message: data.success});
        }).fail(function(data){
            reject({success:false, retorno:data.responseJSON});
        });
    })
};