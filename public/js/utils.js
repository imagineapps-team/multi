$(document).ready(function(){

    $('#confirmDelete').on('show.bs.modal', function (event) {
        var button     = $(event.relatedTarget); 
        
        var registro   = button.data('registro');
        var controller = button.data('control');
        var id         = button.data('id');

        var modal = $(this);
        modal.find('.modal-title').text('Confirmar remoção (' + registro + ')');
        
        var action     = new actions();

        $('#btn-confirm').on('click', function(){
            action.delete('admin/'+controller, id).then(function(result){
                if(result.success){  
                    toast('success', result.message, 3000);
                    modal.modal('hide');
                    $('#'+controller+'_'+id).hide('slow');
                }else{
                    console.log('errou');
                    modal.modal('hide');
                    toast('error', result.message.error, 3000);
                } 
            }).catch(function(error){
                modal.modal('hide');
                toast('error', _formatCatchMsg(error.message.error), 3000);
                return false;
            });
        });
    });

    $('#confirmAction').on('show.bs.modal', function (event) {
        var button     = $(event.relatedTarget); 
        
        var nomeacao   = button.data('nomeacao');
        var controller = button.data('control');
        var acao       = button.data('acao');
        var dados      = button.data('dados');
        
        var modal = $(this);
        modal.find('.modal-title').text('Confirmar ' + nomeacao);
        
        var action     = new actions();
        
        $('#btn-confirm-action').on('click', function(){
            action.specified('admin/'+controller, acao, dados).then(function(result){
                if(result.success){  
                    toast('success', result.message, 2000);
                    setTimeout(location.reload.bind(location), 2000);
                } 
            });
        });
    });

    //Login form
    $('#form-login').on('submit', function (event) {
        $('#btn-submit-text').text('Processando...');
        
        var controller = 'api/oauth/token';
        var data       = $(this).serialize();
        var action     = new actions();
        
        action.login(controller, data).then(function(result){
            if(result.success){
                action.get('admin/loginSaveByToken', result.token).then(function(r){
                    
                    if(r.user !== undefined && r.user.id > 0){
                        $('#btn-submit-text').text('Entrando');
                        toast('success', 'Bem Vindo! Carregando seu ambiente...', 1000);
                        window.location.href='/admin'
                    }

                }).catch(function(result){
                    $('#btn-submit-text').text('Entrar');
                    let r = JSON.parse(result.responseText);
                    toast('error', r.error, 3000);
                });
            }
            return false;
        }).catch(function(result){
            if(!result.success){  
                if(result.retorno.error === 'invalid_credentials'){
                    toast('error', 'Usuário ou senha inválidos', 3000);
                }
                $('#btn-submit-text').text('Entrar');
            }
        });

        return false;
    });

    $('#form').on('submit', function (event) {
        $('#btn-submit').val('Processando...');

        var form       = $(this);
        
        var controller = form.data('control');
        var id         = form.data('registro_id');
        var validation = form.data('validate');
        var data       = $(this).serialize(); 
        var urlRetorno = form.data('url_retorno');

        if(validation !== undefined && validation !== ''){
            var check = execValidationFn(validation, context, data);
            if(check.valid){
                act(controller, data, id, urlRetorno);
                return false;
            }else{
                toast('error', check.msg, 3000);
                $('#btn-submit').val('Salvar');
            }
        }else{
            act(controller, data, id, urlRetorno);
        }

        function act(controller, data, id, urlRetorno){
            var app = new actions();
            var action;
            var retorno = (urlRetorno === undefined) ? controller : urlRetorno; 
            
            action = (id !== undefined && id > 0) ? app.update('admin/'+controller, data, id) : app.insert('admin/'+controller, data);

            action.then(function(result){
                retorno = (urlRetorno === undefined) ? retorno+'/'+result.data.id : retorno;

                toast('success', result.message, 1500, host+'admin/'+retorno);
                //toast('success', result.message, 1500);
                $('#btn-submit').val('Salvar');
            }).catch(function(result){
                console.log(result);
                toast('error', _formatCatchMsg(result.retorno.message), 3000);
                $('#btn-submit').val('Salvar');
            });
        }

        return false;
    });

    $('#searching').keyup(function(e) {
        var controller = $(this).attr('data-control');
        var act        = $(this).attr('data-action');

        if(action === undefined) var action = new actions();

        clearTimeout($.data(this, 'timer'));
        
        if (e.keyCode == 13){
            search(true, action, controller, act);
        }else{
            $(this).data('timer', setTimeout(search(false, action, controller, act)));
        }
          
    });

    $('#titulo, #nome, #sigla').on('change', function(){
        let n = $(this).val(); 
        $(this).val(n.toUpperCase());
    });
    
});

function toast(action, msg, duration, redir = false){
    var x     = document.getElementById("toast-msg");
    var color = '';
    
    switch(action){
        case 'success':
            color = 'green';
            break;
        case 'error':
            color = 'red';
            break;
    }
    
    x.className = "show";
    $('#toast-msg').html(msg);
    $('#toast-msg').css('background-color', color);

    setTimeout(function(){ 
        x.className = x.className.replace("show", "");
        if(redir) window.location.href=redir; 
    }, duration);
};

function search(force, action, controller, act){
    var term = $('#searching').val();
    if (!force && term.length < 3) return;
    
    action.searchInField(force, controller, act, term).then(function(result){
        console.log(result);
        autocomplete(document.getElementById("searching"), result);
    });
};

var resetSelect = function(element){
    $(element)
    .find('option')
    .remove()
    .end()
}

function _formatCatchMsg(msg){
    if(msg !== undefined){
        if(msg.indexOf('usuarios_cpf_unique') !== -1){
            return 'Esse CPF já existe em nosso banco de dados';
        }

        if(msg.indexOf('usuarios_perfil_id_foreign') !== -1){
            return 'Perfil incorreto';
        }

        if(msg.indexOf('usuarios_login_unique') !== -1){
            return 'Esse LOGIN já foi usado';
        }

    }
}