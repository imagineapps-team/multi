var context = {};
context["validadeUsuarioForm"] = function(data){
    data = unserialize(data);

    console.log(data);
    if(data.nome === '')        return { valid:false, msg: 'Preencha o NOME!'};
    
    if(data.cpf === '')         return { valid:false, msg: 'Preencha o CPF'};
    //if(data.cpf.length != 14)   return { valid:false, msg: 'CPF Inválido!'};
    if(!validarCPF(data.cpf))   return { valid:false, msg: 'CPF Inválido!'};
    
    if(data.login === '')       return { valid:false, msg: 'Preencha o login!'};
    if(data.login.length < 5)   return { valid:false, msg: 'O login precisa possui mais que 5 caracteres'};
    
    if(data.senha < 5)          return { vali:false, msg: 'A senha deve possui mais que 5 caracteres'};
    if(data.senha != data.confirm_senha) return { valid:false, msg: 'As senhas não conferem'};
    
    if(data.perfil_id == 0)     return { valid:false, msg: 'Escolha um PERFIL!'};
    if(data.perfil_id == 2 && data.gerente_usuario_id <= 0) return { valid:false, msg: 'Escolha um GERENTE para o usuário'};

    return {valid:true};
};


function validarCPF(strCPF) {
  var Soma;
  var Resto;
  strCPF = strCPF.replace(/(\.|\/|\-)/g,"");

  Soma = 0;
  
  if (strCPF == "00000000000") return false;
   
  for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
  Resto = (Soma * 10) % 11;
 
  if ((Resto == 10) || (Resto == 11))  Resto = 0;
  if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;
 
Soma = 0;
  for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
  Resto = (Soma * 10) % 11;
 
  if ((Resto == 10) || (Resto == 11))  Resto = 0;
  if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
  return true;
}

function unserialize(serialize) {
    let obj = {};
	serialize = serialize.split('&');
	for (let i = 0; i < serialize.length; i++) {
		thisItem = serialize[i].split('=');
		obj[decodeURIComponent(thisItem[0])] = decodeURIComponent(thisItem[1]);
    };
    
	return obj;
}

function execValidationFn(fnName, ctx /*, args */) 
{
  var args = Array.prototype.slice.call(arguments, 2);
  return ctx[fnName].apply(ctx, args);
}