@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dados relevantes</div>

                <div class="card-body">
                    <p><b>{{ $token }}</b> pessoas cadastradas no sistema</p>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
