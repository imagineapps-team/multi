@extends('admin.layouts.app')
@section('content')

            <div class="row valign-wrapper">
                <div class="col s12 valign">
                    <div class="card">
                        <div class="card-content">
                            <p><b> {{ config('app.name')}} </b></p>
                            <p>Ambiente administrativo</p>
                        </div>
                        <div class="card-content grey lighten-4">
                            <form id="form-login">
                                <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="client_id" id="client_id" value="{{ config('app.client_id') }}" />
                                <input type="hidden" name="client_secret" id="client_secret" value="{{ config('app.client_secret') }}" />
                                <input type="hidden" name="grant_type" value="password" />
                                
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="username" type="text" class="validate {{ $errors->has('login') ? ' is-invalid' : '' }}" name="username" required autofocus>
                                        <label for="username">Usuário</label>
                                    </div>
                                </div>  

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="password" type="password" class="validate {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required autofocus>
                                        <label for="password">Senha</label>
                                    </div>
                                </div>

                                <p>
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} />
                                        <span>Lembrar Senha</span>
                                    </label>
                                </p>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <button class="btn waves-effect waves-light" type="submit" id="btn-submit">
                                            <span id="btn-submit-text">Entrar</span>
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
@endsection
