@extends('layouts.app')

@section('content')    
    <h1>Estados do Brasil</h1>  
    
    <hr />
    @if(count($estados) > 0)
        {{ $estados->links() }}
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th scope="col">SIGLA</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Total de Cidades</th>
                </tr>
            </thead>
            <tbody>
            @foreach($estados as $estado)
              <tr>
                <td>{{ $estado->sigla}}</td>
                <td>{{ $estado->nome}}</td>
                <td>{{ $estado->cidades->count()}}</td>

              </tr>  
            @endforeach
            </tbody>  
        </table>
        {{ $estados->links() }}
    @else
        <p>Nennhum estado inserido</p>
    @endif
    <hr />
@endsection