<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>

    <!-- Fonts  -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <!-- Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/main.css?2.3') }}" rel="stylesheet">
    <link href="{{ asset('css/toast.css') }}" rel="stylesheet">
    <script>
        var host = "{{ asset('') }}";
    </script>
</head>
<body>

    <div class="wrapper">
            
@include('admin.inc.menu-left')
    @include('admin.inc.navbar-top')
                <div class="container @if(!Auth::guard('admin')->check()) container-login @endif @if(Auth::guard('admin')->check()) container-logged @endif">
                    @include('admin.inc.messages')

                    @yield('content')

                </div>
    </div>
    @include('admin.inc.modals')
    @include('admin.inc.footer')
</body>
</html>
