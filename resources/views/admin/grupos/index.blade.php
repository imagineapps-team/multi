@extends('layouts.app')

@section('content')    
<h1>Grupos ( {{ number_format($total, 0) }} registros)</h1>  
<button type="button" class="btn btn-success" onclick="window.location.href='./grupos/create'">Cadastrar Novo</button>
    <hr />
    @if(count($grupos) > 0)
        {{ $grupos->links() }}
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Sigla</th>
                    <th scope="col">Total de Pessoas</th>
                    <th scope="col" colspan="2">Ações</th>
                </tr>
            </thead>
            <tbody>
            @foreach($grupos as $grupo)
              <tr id="grupos_{{$grupo->idGrupo}}">
                <td>{{ $grupo->Nome}}</td>
                <td>{{ $grupo->Sigla}}</td>
                <td>{{ $grupo->pessoasCount() }}</td>
                <td><button type="button" class="btn btn-warning" onclick="window.location.href='{{url('grupos/'.$grupo->idGrupo)}}/edit'">Editar</button></td>
                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-registro="{{ $grupo->Nome}}" data-control="grupos" data-id="{{ $grupo->idGrupo}}">Excluir</button></td>
              </tr>  
            @endforeach
            </tbody>  
        </table>
        {{ $grupos->links() }}
    @else
        <p>Nennhum Grupo inserido</p>
    @endif
    <hr />
@endsection