@extends('layouts.app')

@section('content')
    <h1>Editar Grupo</h1>
    
    {!! Form::open(['action' => ['GrupoController@update', $grupo->idGrupo], 'method' => 'PUT']) !!}    
        <div class="form-group">
            {{Form::label('nome','Nome')}}
            {{Form::text('nome',$grupo->Nome, ['class' => 'form-control', 'placeholder' => 'Nome do Grupo'])}}
        </div>

        <div class="form-group">
            {{Form::label('sigla','Sigla')}}
            {{Form::text('sigla',$grupo->Sigla, ['class' => 'form-control', 'placeholder' => 'SIGLA'])}}
        </div>

        <div class="form-group">
            {{Form::label('descricao','Descrição')}}
            {{Form::textarea('descricao', $grupo->Descricao ,['class'=>'form-control', 'rows' => 2, 'cols' => 40]) }}
        </div>
        
        <div class="form-group">
            <p> Pessoas no Grupo: <b>{{ number_format($grupo->pessoasCount(), 0) }}</b></p>
        </div>
        {{Form::hidden('_method','PUT')}}
        
        {{Form::submit('Salvar', ['class' => 'btn btn-success', 'id' => 'btn-submit'])}}
        {{Form::button('Voltar', ['class' => 'btn btn-primary', 'id' => 'voltar', 'onclick' => 'history.back(-1)'])}}    
    {!! Form::close() !!}
@endsection