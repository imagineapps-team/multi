@extends('layouts.app')

@section('content')
    <h1>Cadastrar Novo Grupo</h1>
    
    {!! Form::open(['method' => 'POST', 'id'=>'form', 'data-control' => url('/grupos') ]) !!}    
        <div class="form-group">
            {{Form::label('nome','Nome')}}
            {{Form::text('nome', '' , ['class' => 'form-control', 'placeholder' => 'Nome do Grupo'])}}
        </div>

        <div class="form-group">
            {{Form::label('sigla','Sigla')}}
            {{Form::text('sigla', '', ['class' => 'form-control', 'placeholder' => 'SIGLA'])}}
        </div>

        <div class="form-group">
            {{Form::label('descricao','Descrição')}}
            {{Form::textarea('descricao', '',['class'=>'form-control', 'rows' => 2, 'cols' => 40]) }}
        </div>
        {{Form::submit('Salvar', ['class' => 'btn btn-success', 'id' => 'btn-submit'])}}
        {{Form::button('Voltar', ['class' => 'btn btn-primary', 'id' => 'voltar', 'onblur' => 'history.back(-1)'])}}    
    {!! Form::close() !!}
@endsection