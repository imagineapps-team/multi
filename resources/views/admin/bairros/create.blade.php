@extends('layouts.app')

@section('content')
    <h1>Cadastrar Novo Bairro</h1>
    
    {!! Form::open(['method' => 'POST', 'id'=>'form', 'data-control' => url('/bairros') ]) !!}    
        <div class="form-group">
            {{Form::label('nome','Nome')}}
            {{Form::text('nome', '', ['class' => 'form-control', 'placeholder' => 'Nome do Bairro'])}}
        </div>
        
        <div class="form-group">
            {{Form::label('estado_id','Estado')}}
            {{Form::select('uf_id', $estados, null, ['class' => 'form-control col-6', 'id' => 'estado', 'onclick' => 'getCidades()'])}}
        </div>

        <div class="form-group">
            {{Form::label('cidade_id','Cidade')}}
            {{Form::select('cidade_id', ['0'=>'Escolha um estado'], null, ['class' => 'form-control col-6', 'id'=>'cidades'])}}
        </div>
        
        {{Form::submit('Salvar', ['class' => 'btn btn-success', 'id' => 'btn-submit'])}}
        {{Form::button('Voltar', ['class' => 'btn btn-primary', 'id' => 'voltar', 'onblur' => 'history.back(-1)'])}}    
    {!! Form::close() !!}
@endsection

<script type="text/javascript">
var getCidades = function(){
    var url     = "{{ url('cidades/byuf/') }}"+"/"+$('#estado').val();
    var options = '';  
    $('#cidades').html('<option>AGUARDE</option>');
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json'
    }).done(function(result){
        resetSelect('#cidades');
        result.data.forEach(cidade => {
            $('#cidades').append("<option value='"+cidade.id+"'>"+cidade.nome+"</option>"); 
        });
    }).fail(function(a,b,c){
        console.log(a,b,c);
    });
}

var resetSelect = function(element){
    $(element)
    .find('option')
    .remove()
    .end()
}
</script>