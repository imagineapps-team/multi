@extends('layouts.app')

@section('content')    
<h1>Bairros {{ (isset($cidade)) ? ' da Cidade de '.$cidade['nome'] : ' cadastrados' }} ( {{ number_format($total_registros, 0) }} registros)</h1>
<button type="button" class="btn btn-success" onclick="window.location.href='./bairros/create'">Cadastrar Novo</button>
    <hr />
    @if(count($bairros) > 0)
        Selecionar por estado 
        <div class="row">
        {{ Form::select('estados', $estados, (isset($uf)) ? $uf['id'] : null , ['id' => 'estados', 'class' => 'form-control col-6'])}}
        {{ Form::button('BUSCAR', ['class' => 'btn btn-primary col-2', 'onclick' => 'goUfId()']) }}
        </div>
        
        @if($uf !== null)
            Selecionar por cidade 
            <div class="row">
                {{ Form::select('cidades', $cidades, (isset($cidade)) ? $cidade['id'] : null , ['id' => 'cidades', 'class' => 'form-control col-6'])}}
                {{ Form::button('BUSCAR POR CIDADE', ['class' => 'btn btn-primary col-3', 'onclick' => 'goCidadeId()']) }}
            </div>
        @endif

        <br />
        {{ $bairros->links() }}
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Cidade</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Total de pessoas</th>
                    <th colspan="2" style="text-align:center;">Ação</th>
                </tr>
            </thead>
            <tbody>
            @foreach($bairros as $bairro)
              <tr>
                <td>{{ $bairro->nome}}</td>
                <td><a href={{url('bairros/bycidade/'.$bairro->cidade->id)}}>{{ $bairro->cidade->nome}}</a></td>
                <td><a href={{url('bairros/byuf/'.$bairro->cidade->uf->id)}}>{{ $bairro->cidade->uf->nome}}</a></td>
                <td><a href={{url('pessoas/?bairro_id='.$bairro->id)}}>{{ $bairro->pessoasCount() }}</td>
                <td><button type="button" class="btn btn-warning" onclick="window.location.href='{{url('bairros/'.$bairro->id)}}/edit'">Editar</button></td>
                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-registro="{{ $bairro->nome}}" data-control="bairros" data-id="{{ $bairro->id}}">Excluir</button></td>
              </tr>  
            @endforeach
            </tbody>  
        </table>
        {{ $bairros->links() }}
    @else
        <p>Nennhum bairro inserido</p>
    @endif
    <hr />
@endsection

<script type="text/javascript">
var urlByUf     = "{{ url('bairros/byuf/') }}";
var urlByCidade = "{{ url('bairros/bycidade/') }}";


var goUfId = function(){
    window.location.href=urlByUf+'/'+$('#estados').val();
};

var goCidadeId = function(){
    window.location.href=urlByCidade+'/'+$('#estados').val()+'/'+$('#cidades').val();
};
</script>