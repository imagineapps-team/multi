@extends('layouts.app')

@section('content')
    <h1>Editar Bairro</h1>
    
    {!! Form::open(['action' => ['BairroController@update', $bairro->id], 'method' => 'PUT']) !!}    
        <div class="form-group">
            {{Form::label('nome','Nome')}}
            {{Form::text('nome',$bairro->nome, ['class' => 'form-control', 'placeholder' => 'Nome do Bairro'])}}
        </div>
        <div class="form-group">
            {{Form::label('estado_id','Estado')}}
            {{Form::select('uf_id', $estados, $bairro->cidade->uf->id, ['class' => 'form-control col-6', 'id' => 'estado', 'onclick' => 'getCidades()'])}}
        </div>

        <div class="form-group">
            {{Form::label('cidade_id','Cidade')}}
            {{Form::select('cidade_id', $cidades, $bairro->cidade->id, ['class' => 'form-control col-6', 'id'=>'cidades'])}}
        </div>
        
        <div class="form-group">
        <p> Pessoas no bairro: <b>{{ number_format($bairro->pessoasCount(), 0) }}</b></p>
        </div>
        {{Form::hidden('_method','PUT')}}
        
        {{Form::submit('Salvar', ['class' => 'btn btn-success', 'id' => 'btn-submit'])}}
        {{Form::button('Voltar', ['class' => 'btn btn-primary', 'id' => 'voltar', 'onclick' => 'history.back(-1)'])}}    
    {!! Form::close() !!}
@endsection

<script type="text/javascript">
    var getCidades = function(){
        var url     = "{{ url('cidades/byuf/') }}"+"/"+$('#estado').val();
        var options = '';  
        $('#cidades').html('<option>AGUARDE</option>');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json'
        }).done(function(result){
            resetSelect('#cidades');
            result.data.forEach(cidade => {
                $('#cidades').append("<option value='"+cidade.id+"'>"+cidade.nome+"</option>"); 
            });
        }).fail(function(a,b,c){
            console.log(a,b,c);
        });
    }
    
    var resetSelect = function(element){
        $(element)
        .find('option')
        .remove()
        .end()
    }
    </script>