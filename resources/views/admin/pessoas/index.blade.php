@extends('layouts.app')

@section('content')    
    <h1>Pessoas <button type="button" class="btn btn-success" onclick="window.location.href='./pessoas/create'">Cadastrar Nova</button></h1>      
    <hr />
    @if(count($pessoas) > 0)
        <table id="pessoas-lista" class="display">

        </table>
    @else
        <p>Nennhuma pessoa cadastrada</p>
    @endif
    <button type="button" class="btn btn-success" onclick="window.location.href='./pessoas/create'">Nova pessoa</button>
    <hr>


    <script type="text/javascript">
        var params = {};
            params.url = "{{ url('datatables/pessoas') }}";
            params.columns   = [{title: "ID",       data: "idPessoa"}, 
                                {title: "Nome",     data: "Nome"},  
                                {title: "Gênero",   data: "Sexo"},
                                {title: "Grupo",    data: "Grupo" },
                                {title: "Bairro",   data: "Bairro"},
                                {title: "Cidade",   data: "Cidade"},
                                {title: "Ação", data:null, render:function(data){
                                    var btn_editar = '<button type="button" \n\
                                                              class="btn btn-warning" \n\
                                                              onclick="window.location.href=\'{{ url('pessoas') }}/'+data.idPessoa+'/edit\'"> \n\
                                                            Editar \n\
                                                      </button>';
                                    var btn_excluir = '<button type="button" \n\
                                                               class="btn btn-danger" \n\
                                                               data-toggle="modal" \n\
                                                               data-target="#confirmDelete" \n\
                                                               data-registro="'+data.Nome+'" \n\
                                                               data-control="pessoas" data-id="'+data.idPessoa+'"> \n\
                                                            Excluir \n\
                                                       </button>';                  
                                    return btn_editar+btn_excluir;
                                }}];

            params.dbcolumns = ["pessoas.idPessoa",
                                "pessoas.Nome",
                                "pessoas.Sexo", 
                                "grupos.Nome as Grupo", 
                                "bairros.nome as Bairro",
                                "cidades.nome as Cidade",
                            ];
            params.dbjoins   = [{type:'LEFT',table:'grupos',origin:'pessoas.idGrupo',destiny:'grupos.idGrupo'},
                                {type:'LEFT',table:'bairros',origin:'pessoas.bairro_id',destiny:'bairros.id'},
                                {type:'LEFT',table:'cidades',origin:'bairros.cidade_id',destiny:'cidades.id'}];                  
    </script>
    {!! Html::script('js/pessoas/index.js') !!}
@endsection
