@extends('layouts.app')

@section('content')
    <h1>Nova Pessoa</h1>
    @if(!isset($pessoaEncontrada))
    {!! Form::open(['method' => 'POST', 'action' => 'PessoaController@checkExists' ]) !!}    
        <div class="form-group">
            {{Form::label('grupo','Grupo')}}
            {{Form::select('idGrupo', $grupos, null, ['class' => 'form-control'])}}
        </div>

        <div class="form-row">
            <div class="col-8">
                {{Form::label('nome','Nome')}}
                {{Form::text('Nome','', ['class' => 'form-control'])}}
            </div>
            <div class="col-4">   
                {{Form::label('Sexo','Gênero')}}
                {{Form::select('Sexo', ['M'=>'Masculino','F'=>'Feminino'], null, ['class' => 'form-control'])}}
            </div>
        </div>
        <br />
        
        {{Form::submit('Salvar', ['class' => 'btn btn-success', 'id' => 'btn-submit'])}}
        {{Form::button('Voltar', ['class' => 'btn btn-primary', 'id' => 'voltar', 'onblur' => 'history.back(-1)'])}}

    @else
    {!! Form::open(['method' => 'POST', 'action' => 'PessoaController@store' ]) !!}
        <div class="p-3 mb-2 bg-danger text-white">
            Parece que essa pessoa já existe:<br />
            <p>Código: <b>{{ $pessoaEncontrada->idPessoa }}</b></p>
            <p>Nome: <b>{{ $pessoaEncontrada->Nome }}</b></p>
            <p>Endereço: <b>{{ $pessoaEncontrada->END_Logradouro}}</b></p>
            <p>Bairro: <b>{{ $pessoaEncontrada->bairro->nome }}</b> Cidade/UF: <b>{{ $pessoaEncontrada->bairro->cidade->nome }}/{{ $pessoaEncontrada->bairro->cidade->uf->sigla }}</b></p>
            <p>Telefone: <b>{{ $pessoaEncontrada->Celular1 }}</b></p>
        </div>    
        <div class="form-group">
            {{Form::label('grupo','Grupo')}}
            {{Form::select('idGrupo', $grupos , $pessoaEncontrada->grupo->idGrupo, ['class' => 'form-control'])}}
        </div>
    
        <div class="form-row">
            <div class="col-8">
                {{Form::label('nome','Nome')}}
                {{Form::text('Nome',$pessoaEncontrada->Nome, ['class' => 'form-control'])}}
            </div>
            <div class="col-4">   
                {{Form::label('Sexo','Gênero')}}
                {{Form::select('Sexo', ['M'=>'Masculino','F'=>'Feminino'], $pessoaEncontrada->Sexo, ['class' => 'form-control'])}}
            </div>
        </div>
        <br />
        
        {{Form::submit('Salvar mesmo assim', ['class' => 'btn btn-success', 'id' => 'btn-submit'])}}
        {{Form::button('Cancelar', ['class' => 'btn btn-danger', 'id' => 'voltar', 'onclick' => "window.location.href='".url('pessoas/create')."'"])}}
    @endif    
    {!! Form::close() !!}
@endsection