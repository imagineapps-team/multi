@extends('layouts.app')

@section('content')
    <h1>Editar Pessoa</h1>
    @if(isset($msg_success))
    <div class="p-3 mb-2 bg-success text-white"> {{ $msg_success }}</div>
    @endif
    {!! Form::open(['action' => ['PessoaController@update', $pessoa->idPessoa], 'method' => 'PUT']) !!}    
        <div class="form-group">
            {{Form::label('grupo','Grupo')}}
            {{Form::select('idGrupo', $grupos , is_object($pessoa->grupo) ? $pessoa->grupo->idGrupo : null, ['class' => 'form-control'])}}
        </div>
    
        <div class="form-row">
            <div class="col-6">
                {{Form::label('nome','Nome')}}
                {{Form::text('Nome',$pessoa->Nome, ['class' => 'form-control'])}}
            </div>
            <div class="col-2">   
                {{Form::label('Sexo','Gênero')}}
                {{Form::select('Sexo', ['M'=>'Masculino','F'=>'Feminino'], $pessoa->Sexo, ['class' => 'form-control'])}}
            </div>
            <div class="col-2">   
                {{Form::label('Dt Nascimento','Gênero')}}
                {{Form::date('dtNascimento', $pessoa->dtNascimento, ['class' => 'form-control'])}}
            </div>
            <div class="col-2">   
                {{Form::label('status','Status')}}
                {{Form::select('status', ['A'=>'ATIVO','I'=>'INATIVO'], $pessoa->status, ['class' => 'form-control'])}}
            </div>
        </div>
        <hr>
        <p>Contatos</p>
        <div class="form-group">
            {{Form::label('Email','E-mail')}}
            {{Form::text('Email',$pessoa->Email, ['class' => 'form-control'])}}
        </div>
        <div class="form-row">
            <div class="col-4">   
                {{Form::label('TelefoneFixo','Tel Fixo')}}
                {{Form::text('TelefoneFixo', $pessoa->TelefoneFixo, ['class' => 'form-control telefone'])}}
            </div>
            <div class="col-4">   
                {{Form::label('Celular1','Celular (1)')}}
                {{Form::text('Celular1', $pessoa->Celular1, ['class' => 'form-control celular'])}}
            </div>
            <div class="col-4">   
                {{Form::label('Celular2','Celular (2)')}}
                {{Form::text('Celular2', $pessoa->Celular2, ['class' => 'form-control celular'])}}
            </div>
        </div>
        <hr>
        <p>Endereço</p>
        <div class="form-row">
            <div class="col-3">
                {{Form::label('cep','CEP')}}
                {{Form::text('cep',$pessoa->END_CEP, ['class' => 'form-control cep', 'size'=>'7', 'onkeyup'=>'getAddresByCEP()'])}}
            </div>
        </div>

        <div class="form-row">
            <div class="col-2">
                {{Form::label('uf','Estado')}}
                {{Form::select('estados', $estados, isset($pessoa->bairro) ? $pessoa->bairro->cidade->uf->id : null  , ['id' => 'estados', 'class' => 'form-control', 'onchange' => 'getCidades(null)'])}}
            </div>
            <div class="col-3">
                {{Form::label('cidade','Cidade')}}
                {{ Form::select('cidades', [], null,  ['id' => 'cidades', 'class' => 'form-control', 'onchange' => 'getBairros()'] )}}
            </div>
            <div class="col-3">
                {{Form::label('bairro_id','Bairro')}}
                {{Form::select('bairro_id', $bairros, isset($pessoa->bairro) ? $pessoa->bairro->id : null ,['id' => 'bairro', 'class' => 'form-control'])}}
            </div>
        </div>

        <div class="form-group">
            {{Form::label('logradouro','Logradouro')}}
            {{Form::text('logradouro', $pessoa->END_logradouro , ['id' => 'logradouro', 'class' => 'form-control'])}}
        </div>

        <hr>
        <p>Documentos</p>
        <div class="form-row">
            <div class="col-6">
                {{Form::label('rg','RG')}}
                {{Form::text('rg', $pessoa->RG , ['id' => 'rg', 'class' => 'form-control'])}}
            </div>
            <div class="col-6">
                {{Form::label('rg','CPF')}}
                {{Form::text('cpf', $pessoa->CPF , ['id' => 'cpf', 'class' => 'form-control cpf'])}}
            </div>
        </div>

        <hr>
        <p>Informações Adicionais</p>
        <div class="form-row">
            <div class="col-12">
                {{Form::label('igreja_id','Igreja')}}
                {{Form::select('igreja_id', $igrejas, isset($pessoa->igreja) ? $pessoa->igreja->id : null ,['id' => 'igreja', 'class' => 'form-control'])}}
            </div>
            <div class="col-12">
                {{Form::label('obs','Obs:')}}
                {{Form::textarea('obs', $pessoa->obs , ['id' => 'obs', 'class' => 'form-control'])}}
            </div>    
        </div>

        {{Form::hidden('_method','PUT')}}
        
        {{Form::submit('Salvar', ['class' => 'btn btn-success', 'id' => 'btn-submit'])}}  
        {{Form::button('Voltar', ['class' => 'btn btn-primary', 'id' => 'voltar', 'onclick' => "window.location.href='".url('pessoas')."'"])}}  
        <hr>
    {!! Form::close() !!}
@endsection

<script type="text/javascript">
var getAddresByCEP = function(){
    var cep = $('#cep').val();
    var url = "https://viacep.com.br/ws/"+cep+"/json/";
    
    var urlBairros = "{{ url('bairros/bycidade/') }}";
 
    if(cep.length > 8){
        $.getJSON(url, function(result){
            $("#estados option").filter(function(){
                return $(this).html() == result.uf;
            }).attr('selected', true);
            
            getCidades(result.localidade);

            $.getJSON(urlBairros+'/'+result.uf+'/'+result.localidade, function(rBairros){
                if(rBairros.bairros.length > 0){
                    resetSelect('#bairro');
                    $.each(rBairros.bairros, function(i, bairro){
                        $('#bairro').append("<option value='"+bairro.id+"'>"+bairro.nome+"</option>");
                    });

                    $("#bairro option").filter(function(){
                        var bairro = (result.bairro != undefined) ? result.bairro : '';    
                        return $(this).html() == bairro.toUpperCase();
                    }).attr('selected', true);
                }
            });

            $('#logradouro').val(result.logradouro);
        });
    }

    
};

var getCidades = function(selected){
    var estado = $('#estados').val();
    var url    = "{{ url('cidades/byuf/') }}";

    $.getJSON(url+'/'+estado, function(rCidades){
        if(rCidades.data.length > 0){
            resetSelect('#cidades');
            $.each(rCidades.data, function(i, cidade){
                if(selected !== null && cidade.nome === selected){
                    $('#cidades').append("<option value='"+cidade.id+"' selected>"+cidade.nome+"</option>");
                }else{
                    $('#cidades').append("<option value='"+cidade.id+"'>"+cidade.nome+"</option>");
                }             
            });
        }
    });

};

var getBairros = function(){
    var estado = $('#estados').val();
    var cidade = $('#cidades').val();
    var url    = "{{ url('bairros/bycidade/') }}";

    $.getJSON(url+'/'+estado+'/'+cidade, function(rBairros){
        if(rBairros.data.length > 0){
            resetSelect('#bairro');
            $.each(rBairros.data, function(i, bairro){
                $('#bairro').append("<option value='"+bairro.id+"'>"+bairro.nome+"</option>");
            });
        }

    });

};

var resetSelect = function(element){
    $(element)
    .find('option')
    .remove()
    .end()
}
</script>