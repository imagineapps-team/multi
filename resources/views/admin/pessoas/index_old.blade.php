@extends('layouts.app')

@section('content')    
    <h1>Pessoas <button type="button" class="btn btn-success" onclick="window.location.href='./pessoas/create'">Cadastrar Nova</button></h1>  
    
    <hr />
    @if(count($pessoas) > 0)
        {{ $pessoas->links() }}
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Gênero</th>
                    <th scope="col">Grupo</th>
                    <th scope="col">Email</th>
                    <th scope="col">Bairro</th>
                    <th scope="col">Cidade/UF</th>
                    <th colspan="2" style="text-align:center;">Ação</th>
                </tr>
            </thead>
            <tbody>
            @foreach($pessoas as $pessoa)
                <tr id="pessoas_{{$pessoa->idPessoa}}">
                    <td>{{ $pessoa->Nome}}</td>
                    <td>{{ $pessoa->Sexo}}</td>
                    <td>{{ $pessoa->grupo->Nome }}</td>
                    <td>{{ $pessoa->Email}}</td>
                    <td>@if($pessoa->bairro) {{ $pessoa->bairro->nome }} @endif</td>
                    <td>@if($pessoa->bairro) {{ $pessoa->bairro->cidade->nome}} / {{ $pessoa->bairro->cidade->uf->sigla}} @endif</td>
                    <td><button type="button" class="btn btn-warning" onclick="window.location.href='{{url('pessoas/'.$pessoa->idPessoa)}}/edit'">Editar</button></td>
                    <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-registro="{{ $pessoa->Nome}}" data-control="pessoas" data-id="{{ $pessoa->idPessoa}}">Excluir</button></td>
                </tr>
            @endforeach
            </tbody>  
        </table>
        {{ $pessoas->links() }}

    @else
        <p>Nennhuma pessoa cadastrada</p>
    @endif
    <button type="button" class="btn btn-success" onclick="window.location.href='./pessoas/create'">Nova pessoa</button>
    <hr>
@endsection
