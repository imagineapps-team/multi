@extends('layouts.app')

@section('content')
    <h1>Relatórios > Etiquetas</h1>
    <hr />  
    {!! Form::open(['method' => 'POST', 'action' => 'RelatoriosController@gerar', 'name'=>'relatorio', 'id'=>'form-relatorio']) !!}    
        <div class="form-group">
            {{Form::label('genero','Gênero')}}
            {{Form::select('sexo', ['T'=> 'Todos', 'M'=>'Masculino', 'F'=>'Feminino'] , 'T', ['id' => 'genero', 'class' => 'form-control'])}}
        </div>

        <div class="form-group">
            {{Form::label('grupo','Grupo')}}
            {{Form::select('idGrupo', $grupos , null, ['id' => 'grupo', 'class' => 'form-control'])}}
        </div>

        <div class="form-group">
            {{ Form::label('estado','Estado')}}
            {{ Form::select('estados', $ufs , null, ['id' => 'estados', 'class' => 'form-control', 'onchange'=> 'getCidades()'])        }}
        </div>

        <div class="form-group">
            {{Form::label('cidade','Cidade')}}
            {{Form::select('cidade', [false=>'Selecione um estado'] , null, ['id' => 'cidades', 'class' => 'form-control', 'onchange' => 'getBairros()'])}}
        </div>

        <div class="form-group">
            {{Form::label('bairro','Bairro')}}
            {{Form::select('bairro', [false=>'Selecione uma Cidade'] , null, ['id' => 'bairros', 'class' => 'form-control', 'multiple'=>'multiple'])}}
        </div>

        <div class="form-group">
            {{Form::label('cols','Colunas')}}
            {{Form::select('colunas', [2=>'Duas', 3=>'Três', 4=>'Quatro'], null, ['class' => 'form-control'])}}
        </div>

        {{ Form::hidden('tiporelatorio', 'etiquetas') }}
        {{ Form::hidden('formato', null, ['id' => 'formato']) }}

        {{Form::button('Ver Impressão', ['class' => 'btn btn-primary', 'id' => 'html', 'onclick'=>'gerarHTML()'])}}
        
    {!! Form::close() !!}    
@endsection
<script>
    var gerarXLS = function(){
        $('#formato').val('XLS');
        document.relatorio.submit();
    };

    var gerarHTML = function(){
        window.open('./html/etiquetas/?'+$('#form-relatorio').serialize(),'_blank');
    }

    var showOpcs = function(){
        var modelo = $('#modelo').val();
        switch(modelo){
            case 'ET':
                $('#opcs_model_etiquetas').show();
                break;
            case 'LT-S':
                $('#opcs_model_etiquetas').hide();
                break;
        }
    };

    var getCidades = function(){
        var url     = "{{ url('cidades/byuf/') }}"+"/"+$('#estados').val();
        var options = '';  
        $('#cidades').html('<option>AGUARDE</option>');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json'
        }).done(function(result){
            resetSelect('#cidades');
            $('#cidades').append("<option value='T'>TODAS</option>");
            result.data.forEach(cidade => {
                $('#cidades').append("<option value='"+cidade.id+"'>"+cidade.nome+"</option>"); 
            });
        }).fail(function(a,b,c){
            console.log(a,b,c);
        });
    }

    var getBairros = function(){
        var estado = $('#estados').val();
        var cidade = $('#cidades').val();
        var url    = "{{ url('bairros/bycidade/') }}";

        $.getJSON(url+'/'+estado+'/'+cidade, function(rBairros){
            
            if(rBairros.data.length > 0){
                resetSelect('#bairros');
                $('#bairros').append("<option value='T'>TODOS</option>");
                $.each(rBairros.data, function(i, bairro){
                    $('#bairros').append("<option value='"+bairro.id+"'>"+bairro.nome+"</option>");
                });
            }

        });

    };

    var resetSelect = function(element){
        $(element)
        .find('option')
        .remove()
        .end()
    }
</script>