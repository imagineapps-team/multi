@extends('layouts.app')

@section('content')
    <h1>Relatórios > Dados Gerais</h1>
    <hr />

    {!! Form::open(['method' => 'POST', 'action' => 'RelatoriosController@gerar', 'name'=>'relatorio']) !!}    
        <div class="form-group">
            {{Form::label('grupo','Grupo')}}
            {{Form::select('idGrupo', $grupos , null, ['id' => 'grupo', 'class' => 'form-control'])}}
        </div>

        <div class="form-group">
            {{Form::label('mes_aniv','Mês Aniversário')}}
            {{Form::select('mes_aniv', ['t'=>'TODOS',
                                        '01'=>'Janeiro',
                                        '02'=>'Fevereiro',
                                        '03'=>'Março',
                                        '04'=>'Abril',
                                        '05'=>'Maio',
                                        '06'=>'Junho',
                                        '07'=>'Julho',
                                        '08'=>'Agosto',
                                        '09'=>'Setembro',
                                        '10'=>'Outubro',
                                        '11'=>'Novembro',
                                        '12'=>'Dezembro'] , null, ['id' => 'grupo', 'class' => 'form-control'])}}
        </div>

        <div class="form-group" id="opcs_model_etiquetas" style="display:none;">
            {{Form::label('cols','Colunas')}}
            {{Form::select('colunas', [2=>'Duas', 3=>'Três', 4=>'Quatro'], null, ['class' => 'form-control'])}}
        </div>
        {{ Form::hidden('tiporelatorio', 'dadosgerais') }}
        {{ Form::hidden('formato', null, ['id' => 'formato']) }}

        {{Form::button('Gerar XLS', ['class' => 'btn btn-success', 'id' => 'xls', 'onclick'=>'gerarXLS()'])}}
        {{Form::button('Gerar PDF', ['class' => 'btn btn-danger', 'id' => 'pdf'])}}
        {{Form::button('Ver Impressão', ['class' => 'btn btn-primary', 'id' => 'html'])}}
        
    {!! Form::close() !!}    
@endsection
<script>
    var gerarXLS = function(){
        $('#formato').val('XLS');
        document.relatorio.submit();
    };

    var showOpcs = function(){
        var modelo = $('#modelo').val();
        switch(modelo){
            case 'ET':
                $('#opcs_model_etiquetas').show();
                break;
            case 'LT-S':
                $('#opcs_model_etiquetas').hide();
                break;
        }
    };
</script>