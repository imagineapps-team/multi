<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Etiquetas</title>
        <style>
        table { font-family: Verdana, Geneva, sans-serif; font-size:9px; }
        @media print {
            table {page-break-inside: avoid;}
        }
        </style>
    </head>

    <body>
    <?php
    $c = 0; $p = 0;
    if($dados){
    foreach($dados as $pessoa){
        $endereco_linha1 = $pessoa->END_logradouro.', '.$pessoa->bairro; 
        $endereco_linha2 = $pessoa->cidade.'/'.$pessoa->uf;
        
        (strlen($endereco_linha1) > 20) ? $class_end1 = 'font-size:7px;' : $class_end1 = 'font-size:9px;';
        (strlen($endereco_linha2) > 20) ? $class_end2 = 'font-size:7px;' : $class_end2 = 'font-size:9px;';
            
        
        if($p == 0){
    ?>
        <table width="108%" style="margin-left: -15px" border="0" cellspacing="8">
            <tr style="height: 50px;">
    <?php
        }    
        $c++;
        if($c <= $colunas){
             $p++;
    ?>
                <td width="35%" ><p>Destinatário:<br />
                <?php echo  $pessoa->Nome;?><br />
                
                <span style='<?php echo $class_end1; ?>'><?php echo $endereco_linha1; ?></span><br />
                <span style='<?php echo $class_end2; ?>'><?php echo $endereco_linha2; ?></span>
                CEP <?php echo $pessoa->END_CEP;?></p></td>
    <?php
        }else{
    ?>
            </tr>
            <!--
            <tr style="height: 25px;">
                <td></td>
                <td></td>
                <td></td>
            </tr>
            -->
            <tr style="height: 95px;">
    <?php
            $c = 0;
        }
        if($p == 30){
    ?>
        </tr>
        </table>
        <pagebreak />
        <table width="109%" border="0">
    <?php
            $p = 0;$c = 0;
        }
    }
    }
    ?>
    </body>
</html>