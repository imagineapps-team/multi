@extends('layouts.app')

@section('content')    
<h1>Cidades {{ (isset($uf)) ? 'do estado de '.$uf['nome'] : 'do Brasil' }} ( {{ number_format($total_registros, 0) }} registros)</h1>  
    <hr />
    @if(count($cidades) > 0)
        Selecionar por estado 
        <div class="row">
        {{ Form::select('estados', $estados, (isset($uf)) ? $uf['id'] : null , ['id' => 'estados', 'class' => 'form-control col-6'])}}
        {{ Form::button('BUSCAR', ['class' => 'btn btn-primary col-2', 'onclick' => 'goUfId()']) }}
        </div>
        <br />
        {{ $cidades->links() }}
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Total de Bairros</th>
                </tr>
            </thead>
            <tbody>
            @foreach($cidades as $cidade)
              <tr>
                <td>{{ $cidade->nome}}</td>
                <td><a href={{url('cidades/byuf/'.$cidade->uf->id)}}>{{ $cidade->uf->nome}}</a></td>
                <td>{{ $cidade->bairros->count() }}</td>

              </tr>  
            @endforeach
            </tbody>  
        </table>
        {{ $cidades->links() }}
    @else
        <p>Nennhuma cidade inserida</p>
    @endif
    <hr />
@endsection

<script type="text/javascript">
var url = "{{ url('cidades/byuf/') }}";

var goUfId = function(){
    window.location.href=url+'/'+$('#estados').val();
};
</script>