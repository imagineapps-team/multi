@extends('admin.layouts.app')

@section('content')
    <h1> 
        <?php echo isset($usuario) ? 'Editar' : 'Cadastrar'; ?> 
        Usuário
    </h1>
    {!! Form::open(['method' => 'POST', 
                    'id'     =>'form', 
                    'data-model' => 'Usuario',
                    'data-control' => 'usuarios', 
                    'data-validate'=>'validadeUsuarioForm', 
                    'data-registro_id' => isset($usuario) ? $usuario->id : null,
                    'data-url_retorno' => isset($usuario) ? 'admin/usuarios/?perfil_id='.$usuario->perfil->id : 'admin/usuarios/?perfil_id=1']) !!}    
        <div class="form-row">
            {{ Form::label('pessoa', 'Pessoa')}} 
            {{ Form::text('pessoa', null, ['class'=>'form-control', 'id'=>'pessoa', 'placeholder'=>'Pesquise um pessoa...'])}}               
        </div>

        
        <div class="form-row">
            <div class="col-6">
                {{Form::label('perfil','Perfil')}}
                {{Form::select('perfil_id', $perfis, isset($usuario) ? $usuario->perfil->id : null, ['id'=>'perfil_id', 'class' => 'form-control', 'onchange'=>'checarPerfil()'])}}
            </div>
        </div>
        
        <hr />
        <div class="form-row">
            <div class="col-4">   
                {{Form::label('login','Login')}}
                {{Form::text('login', isset($usuario) ? $usuario->login : null, ['class' => 'form-control'])}}
            </div>
            
            <?php if(!isset($usuario)){ ?>
            <div class="col-4">   
                {{Form::label('senha','Senha')}}
                {{Form::password('senha', null, ['class' => 'form-control'])}}
            </div>
            
            <div class="col-4">   
                {{Form::label('repita','Repita')}}
                {{Form::password('confirm_senha', null, ['class' => 'form-control'])}}
            </div>
            <?php } ?>
        </div>
        <hr />
        {{Form::submit('Salvar', ['class' => 'btn btn-success', 'id' => 'btn-submit'])}}
        {{Form::button('Cancelar', ['class' => 'btn btn-danger', 'id' => 'voltar', 'onclick' => "window.location.href='".url('usuarios')."'"])}}

        <?php if(isset($usuario)){ ?>
            <button type="button" class="btn btn-warning">
                <i class="material-icons">vpn_key</i>                    
                Alterar senha
            </button>
        <?php } ?>   
        
    {!! Form::close() !!}    
    
@endsection

<script type="text/javascript">

let checarPerfil = function(){
    let perfil = $('#perfil_id').val();

    parseInt(perfil) === 2 ? $('#exibeGerente').show() : $('#exibeGerente').hide() 
};

</script>