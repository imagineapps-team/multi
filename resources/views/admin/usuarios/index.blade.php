@extends('layouts.app')

@section('content')
<br clear="all" />
<div class="container">    
    <h2> Usuários > {{ $titulo }} <button type="button" class="btn btn-success" onclick="window.location.href='{{ url('admin/usuarios/create')}}'">Cadastrar Novo</button></h2>  
    <h5 class="alert-info"> {{ $usuarios['meta']['pagination']['total'] }} registros</h5>

    <hr />
    @if(count($usuarios) > 0)
        @include('inc.grid.index', ['controller' => 'usuarios', 
                              'collection' => $usuarios, 
                              'fields'     => $fields,
                              'display'    => 'nome',
                              'buttons'    => ['edit', 'delete']])  
    @else
        <p>Nennhum usuário cadastrado</p>
    @endif
    <button type="button" class="btn btn-success" onclick="window.location.href='{{ url('admin/usuarios/create')}}'">Novo Usuário</button>
    <hr>
</div>
@endsection
