@if(Auth::guard('admin')->check())

        <div class="sidebar">
            <ul>
                <li>
                    <a>
                        <div class="logo"></div>
                        <span class="text logo-name">
                            Imagine Apps
                        </span>
                    </a>
                </li>
                <li class="separator"></li>
                <li>
                    <a href="{{ url('admin/usuarios?perfil_id=3') }}">
                        <i class="small material-icons fix-vertical">assignment_ind</i>
                        <span class="text">
                            Usuário (Sistema)
                        </span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('admin/usuarios?perfil_id=2') }}">
                        <i class="small material-icons fix-vertical">motorcycle</i>
                        <span class="text">
                            Aluno
                        </span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('admin/usuarios?perfil_id=1') }}">
                        <i class="small material-icons fix-vertical">touch_app</i>
                        <span class="text">
                            Professor
                        </span>
                    </a>
                </li>
            </ul>
        </div>
@endif