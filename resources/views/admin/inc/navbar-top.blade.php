
<nav>
    <div class="nav-wrapper">
        <ul class="left @if(Auth::guard('admin')->check()) sideon @endif">
@if(Auth::guard('admin')->check())

            <li>
                <a href="{{ url('admin') }}"><i class="material-icons">accessible</i></a>
            </li>            
            <li>
                <a href="{{ url('admin') }}"><i class="material-icons">bookmark</i></a>
            </li>            
            <li>
                <a href="{{ url('admin') }}"><i class="material-icons">call</i></a>
            </li>            
            <li>
                <a href="{{ url('admin') }}"><i class="material-icons">cloud</i></a>
            </li>
@endif
@if(!Auth::guard('admin')->check())
            <li>
                <a href="{{ url('admin') }}"> Entrar </a>
            </li>
@endif
        </ul>
        <ul class="right">
@if(Auth::guard('admin')->check())

            <li>
                <a href="{{ url('admin') }}"><i class="material-icons">accessible</i></a>
            </li>            
            <li>
                <a href="{{ url('admin') }}"><i class="material-icons">bookmark</i></a>
            </li>            
            <li>
                <a href="{{ url('admin') }}"><i class="material-icons">call</i></a>
            </li>            
            <li>
                <a href="{{ url('admin') }}"><i class="material-icons">cloud</i></a>
            </li>
            @if(Auth::guard('admin')->check())  <li class="ft"><a href="{{ url('admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sair <span class="mbri-logout al"></span></a>
                <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
                </form>
                </li>
                @endif
@endif
        </ul>
    </div>
</nav>


{{-- <nav>
        <div class="nav-wrapper">
            <ul class="left">
                @if(Auth::guard('admin')->check()) <li class="ft button"><a id="sidebarButton" class=" btn btn-square waves-effect waves-light dashLightColor"><span class="mbri-menu"></span></a></li>
                @endif <li class="mutable fm"><a href="#"><div class="logo"></div></a></li>
                @if(!Auth::guard('admin')->check()) <li class="ft"><a href="{{ url('admin') }}">Entrar</a></li>
                @endif
            
            </ul>

            <ul class="right">
                @if(Auth::guard('admin')->check()) <li class="mutable ft "><a href="#">Bem vindo {{ Auth::guard('admin')->user()->pessoa->nome }}</a></li>
                @endif <li class="muted fm"><a href="#"><div class="logo"></div></a></li>
                @if(Auth::guard('admin')->check())  <li class="ft"><a href="{{ url('admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sair <span class="mbri-logout al"></span></a>
                <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
                </form>
                </li>
                @endif

            </ul>
        </div>
    </nav> --}}