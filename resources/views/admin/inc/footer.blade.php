<div id="toast-msg"></div>
<!-- Scripts -->
<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/validations.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/input-mask.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap/bootstrap.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/materialize.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/autocomplete.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/prototype.js?22') }}"></script>
<script type="text/javascript" src="{{ asset('js/utils.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

<script type="text/javascript">
  $('.cep').mask('00000-000');
  $('.celular').mask('(00)0 0000 0000');
  $('.telefone').mask('(00)0000 0000');
  $('.cpf').mask('000.000.000-00');
</script>