@if(is_array($pagination['links']))
    <ul class="pagination">
    @foreach($pagination['links'] as $chave=>$link)
        <li class="pagination"><a href="{{ $link }}"> {{ $chave }} </a></li>
    @endforeach
    </ul>
@endif