<div class="card">
    <div class="card-body">
        <h5 class="card-title">Buscar [<a href="#" id="expandir" onclick="expandir()">+</a>]</h5>
        <div class="row" id="form-busca" style="display:none;">
            <div class="col-sm">
                <label for="filtro-busca">Filtro</label>
                <select id="filtro-busca" name="filtro-busca" class="form-control">
                    @foreach ($fields as $chave=>$valor)
                    <option value="{{ is_array($valor) ? end($valor) : $valor }}"> {{ $chave }}</option>
                    @endforeach    
                </select>
            </div>
            <div class="col-sm">
                <label for="filtro-texto">pesquisar por</label>
                <input type="text" name="filtro-texto" id="filtro-texto" placeholder="Escreva o texto da busca..." class="form-control" />
            </div>
            <div class="col-sm">
                <button type="button" id="filtro-buscar" name="filtro-buscar" class="btn btn-success" onclick="busca()">
                    <i class="small material-icons">search</i>
                    Pesquisar
                </button>
            </div>
        </div>   
    </div>
</div>

<script>
let busca = function(){
    // Ver a busca para resscrever a url toda vez
    var filtro = $('#filtro-busca').val();
    var valor  = $('#filtro-texto').val();
    
    window.location.href=host+'admin/<?=$controller;?>?search='+valor;
};

let expandir = function(){
    var display = $('#form-busca').css('display');

    if(display === 'none'){
        $('#form-busca').show();
        $('#expandir').text('-');
    }else{
        $('#form-busca').hide();
        $('#expandir').text('+');
    }
    
};
</script>