@include('admin.inc.grid.search', ['fields' => $fields, 'controller' => $controller])

@include('admin.inc.grid.pagination', ['pagination' => $collection['meta']['pagination']])

<table class="table table-hover table-striped">
    <thead>
        <tr>
            @foreach ($fields as $coluna=>$valor)
                <th scope="col"> {{ $coluna }}</th>       
            @endforeach

            <th colspan=" {{ is_array($buttons) ? count($buttons) : 2 }}" style="text-align:center;">Ação</th>
        </tr>
    </thead>
    <tbody>   
    @foreach($collection['data'] as $item)
        <?php 

        $obj = (object) $item;
        
        if(isset($obj->status) && in_array('status', $fields)){
            if($obj->status === 'P'){ 
                $class  = "table-info"; 
                $status = "Pendente"; 
            }elseif($obj->status === 'I'){
                $class  = "table-danger";
                $status = "Inativo"; 
            }elseif($obj->status === 'A'){
                $class = "";
                $status = "Ativo";
            }
        }else{
            $class = "";
        }  
        ?>
        <tr id="{{$controller}}_{{$obj->id}}" class="{{ $class }}">
            @foreach ($fields as $coluna=>$valor)
                @if($valor === 'status')
                    <td>{{ $status }}</td> 
                @else
                    @if(is_array($valor))
                        <td>
                        @foreach ($valor as $item)
                            {{ $obj->$item.' ' }}        
                        @endforeach
                        </td>
                    @else
                        <td> {{ $obj->$valor }}</td>
                    @endif
                @endif           
            @endforeach
            
            @if(is_array($buttons) && in_array('edit', $buttons))
                <td><button type="button" class="btn btn-warning" onclick="window.location.href='{{url('admin/'.$controller.'/'.$obj->id)}}'">Editar</button></td>         
            @endif

            @if(is_array($buttons) && in_array('delete', $buttons))
            <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-registro="{{ $obj->$display}}" data-control="{{ $controller }}" data-id="{{ $obj->id}}">Excluir</button></td>         
            @endif
        </tr>
    @endforeach 
    </tbody>
</table>

@include('admin.inc.grid.pagination', ['pagination' => $collection['meta']['pagination']])
