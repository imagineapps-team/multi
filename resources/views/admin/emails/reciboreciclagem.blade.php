<h3>Olá <i>{{ $reciclagem->participacao->contrato->nomeTitular }}</i></h3>
<p>Sua reciclagem foi realizada com sucesso! Veja abaixo os detalhes da operação:</p>
<hr />
<p>NUC: <b>{{ $reciclagem->participacao->contrato->numero }}</b></p>
<p>Data e Hora: <b>{{ $reciclagem->data_hora }}</b></p>
<p>Código: <b>{{ $reciclagem->codigo }}</b></p>
<p>Bônus total: <b>{{ $reciclagem->bonus_total }}</b></p>
<p>:::RESÍDUOS:::</p>
<ul>
    @foreach($reciclagem->residuos as $residuo)
    <li>{{ $residuo->recurso->nome }} | {{ $residuo->quantidade }} KG. <b>Total de R$ {{ $residuo->bonus_valor }}</b></li>
    @endforeach
</ul>
<img src="http://lightrecicla.avsi.eco.br/img/logo-light-recicla.jpg" width="320px" align="center" />
<hr />
<b style="color:red">Informações Importantes!</b>
<em><li>Este e-mail é automático e não deve ser respondido.</li></em>
<em><li>Você está recebendo esta mensagem porque optou por e-mail como forma de recibo da reciclagem acima descrita.</li> </em>
<em><li>Caso você não seja {{ $reciclagem->participacao->contrato->nomeTitular }}, favor desconsiderar esta mensagem. </li></em>
<br>
<i style="color:green">LIGHTRECICLA</i>
<br><b>O Planeta Agradece ;)</b>

