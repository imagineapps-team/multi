<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\LanceRepository;
use App\Entities\Lance;

/**
 * Class LanceRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class LanceRepositoryEloquent extends BaseRepository implements LanceRepository
{
    use TraitLog;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Lance::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
