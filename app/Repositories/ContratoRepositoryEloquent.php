<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ContratoRepository;
use App\Entities\Contrato;

/**
 * Class ContratoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ContratoRepositoryEloquent extends BaseRepository implements ContratoRepository
{
    use TraitLog;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Contrato::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
