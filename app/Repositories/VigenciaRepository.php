<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VigenciaRepository.
 *
 * @package namespace App\Repositories;
 */
interface VigenciaRepository extends RepositoryInterface
{
    //
}
