<?php
namespace App\Repositories;

use App\Entities\Modulo;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class ModuloRepositoryEloquent extends BaseRepository implements ModuloRepository
{
    use TraitLog;

    protected $fieldSearchable = [
        'nome' => 'like',
        'sigla',
        'status'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Modulo::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function list($withSigla = false){
        $modulos = Modulo::where(['status' => 'A'])->get();
        $list = [];
        if(is_object($modulos) && count($modulos) > 0){
            foreach($modulos as $modulo){
                if($withSigla === 'SIGLA'):
                    $list[$modulo->sigla] = $modulo->nome;
                else:
                    $list[$modulo->id] = $modulo->nome;
                endif;

                
            }
        }
        
        return $list;
    }

}