<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReciclagemRepository.
 *
 * @package namespace App\Repositories;
 */
interface ReciclagemRepository extends RepositoryInterface
{
    //
}
