<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ReciclagemRecursoRepository;
use App\Entities\ReciclagemRecurso;
use App\Validators\ReciclagemRecursoValidator;

/**
 * Class ReciclagemRecursoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ReciclagemRecursoRepositoryEloquent extends BaseRepository implements ReciclagemRecursoRepository
{
    use TraitLog;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ReciclagemRecurso::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
