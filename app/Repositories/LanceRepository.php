<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LanceRepository.
 *
 * @package namespace App\Repositories;
 */
interface LanceRepository extends RepositoryInterface
{
    //
}
