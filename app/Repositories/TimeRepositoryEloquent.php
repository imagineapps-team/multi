<?php
namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\TimeRepository;
use App\Presenters\TimePresenter;
use App\Entities\Time;

class TimeRepositoryEloquent extends BaseRepository implements TimeRepository
{
    use TraitLog;

    protected $fieldSearchable = [
        'nome' => 'like',
        'sigla',
        'status'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Time::class;
    }

    /**
     * @return string
     */
    public function presenter()
    {
        return TimePresenter::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    
}