<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ParticipacaoRepository.
 *
 * @package namespace App\Repositories;
 */
interface ParticipacaoRepository extends RepositoryInterface
{
    //
}
