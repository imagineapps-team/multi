<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReciclagemRecursoRepository.
 *
 * @package namespace App\Repositories;
 */
interface ReciclagemRecursoRepository extends RepositoryInterface
{
    //
}
