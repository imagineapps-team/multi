<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RodadaRepository.
 *
 * @package namespace App\Repositories;
 */
interface RodadaRepository extends RepositoryInterface
{
    //
}
