<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RodadaRepository;
use App\Entities\Rodada;
use App\Validators\RodadaValidator;
use App\Presenters\RodadaPresenter;

/**
 * Class RodadaRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RodadaRepositoryEloquent extends BaseRepository implements RodadaRepository
{
    use TraitLog;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Rodada::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RodadaValidator::class;
    }

    /**
     * @return string
     */
    public function presenter()
    {
        return RodadaPresenter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
