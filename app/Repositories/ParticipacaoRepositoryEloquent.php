<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ParticipacaoRepository;
use App\Entities\Participacao;
use App\Validators\ParticipacaoValidator;

/**
 * Class ParticipacaoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ParticipacaoRepositoryEloquent extends BaseRepository implements ParticipacaoRepository
{
    use TraitLog;

    protected $fieldSearchable = [
        'cliente_id',
        'cliente.nome',
        'contrato_id',
        'contrato.numero',
        'evento_id',
        'status'
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Participacao::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
