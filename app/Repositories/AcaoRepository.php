<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AcaoRepository.
 *
 * @package namespace App\Repositories;
 */
interface AcaoRepository extends RepositoryInterface
{
    //
}
