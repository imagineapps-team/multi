<?php
namespace App\Repositories;

use App\Entities\Perfil;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class PerfilRepositoryEloquent extends BaseRepository implements PerfilRepository
{
    use TraitLog;

    protected $fieldSearchable = [
        'nome' => 'like',
        'sigla',
        'status'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Perfil::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function list($withSigla = false){
        $perfis = Perfil::where(['status' => 'A'])->get();
        $list = [];
        if(is_object($perfis) && count($perfis) > 0){
            foreach($perfis as $perfil){
                if($withSigla === 'SIGLA'):
                    $list[$perfil->sigla] = $perfil->nome;
                else:
                    $list[$perfil->id] = $perfil->nome;
                endif;
            }
        }
        
        return $list;
    }

}