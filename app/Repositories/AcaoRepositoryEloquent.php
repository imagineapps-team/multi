<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AcaoRepository;
use App\Entities\Acao;
use App\Validators\AcaoValidator;

/**
 * Class AcaoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AcaoRepositoryEloquent extends BaseRepository implements AcaoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Acao::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
