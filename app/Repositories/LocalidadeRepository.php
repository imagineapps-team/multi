<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LocalidadeRepository.
 *
 * @package namespace App\Repositories;
 */
interface LocalidadeRepository extends RepositoryInterface
{
    //
}
