<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\VigenciaRepository;
use App\Entities\Vigencia;
use App\Validators\VigenciaValidator;

/**
 * Class VigenciaRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class VigenciaRepositoryEloquent extends BaseRepository implements VigenciaRepository
{
    use TraitLog;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Vigencia::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
