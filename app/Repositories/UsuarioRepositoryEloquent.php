<?php
namespace App\Repositories;

use App\Entities\Aplicacao;
use App\Entities\Usuario;
use App\Presenters\UsuarioPresenter;
use App\Criteria\FindUsuarioByPerilCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class UsuarioRepositoryEloquent extends BaseRepository implements UsuarioRepository
{
    use TraitLog;

    protected $fieldSearchable = [
        'login',
        'nome' => 'like',
        'cpf' => 'like',
        'status'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Usuario::class;
    }


    /**
     * @return string
     */
    public function presenter()
    {
        return UsuarioPresenter::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(app(FindUsuarioByPerilCriteria::class));
    }
    
    public function cambistasCount(){
        $this->skipPresenter(true);
        $count =  $this->cambistas()->selectRaw('gerente_usuario_id, count(*) as total')->groupBy('gerente_usuario_id');
        return is_object($count->first()) ? $count->first()->total : 0;
    }
}