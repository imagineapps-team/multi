<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ReciclagemRepository;
use App\Entities\Reciclagem;
use App\Validators\ReciclagemValidator;

/**
 * Class ReciclagemRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ReciclagemRepositoryEloquent extends BaseRepository implements ReciclagemRepository
{
    use TraitLog;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Reciclagem::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
   
}
