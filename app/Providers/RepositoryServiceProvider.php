<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\UsuarioRepository::class, \App\Repositories\UsuarioRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ContratoRepository::class, \App\Repositories\ContratoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ClienteRepository::class, \App\Repositories\ClienteRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EventoRepository::class, \App\Repositories\EventoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ParticipacaoRepository::class, \App\Repositories\ParticipacaoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\VigenciaRepository::class, \App\Repositories\VigenciaRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ReciclagemRepository::class, \App\Repositories\ReciclagemRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CampanhaRepository::class, \App\Repositories\CampanhaRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ApostaRepository::class, \App\Repositories\ApostaRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\LanceRepository::class, \App\Repositories\LanceRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ConfiguracaoRepository::class, \App\Repositories\ConfiguracaoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RodadaRepository::class, \App\Repositories\RodadaRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\TimeRepository::class, \App\Repositories\TimeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PessoaRepository::class, \App\Repositories\PessoaRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EnderecoRepository::class, \App\Repositories\EnderecoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AlunoRepository::class, \App\Repositories\AlunoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ModuloRepository::class, \App\Repositories\ModuloRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PerfilRepository::class, \App\Repositories\PerfilRepositoryEloquent::class);        
        //:end-bindings:
    }
}
