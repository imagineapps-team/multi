<?php
namespace App\Presenters;

use App\Transformers\RodadaTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TimePresenter
 *
 * @package namespace App\Presenters;
 */
class RodadaPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RodadaTransformer();
    }
}
