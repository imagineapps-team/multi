<?php

namespace App\Presenters;

use App\Transformers\EnderecoTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EnderecoPresenter.
 *
 * @package namespace App\Presenters;
 */
class EnderecoPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EnderecoTransformer();
    }
}
