<?php
namespace App\Presenters;

use App\Transformers\TimeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TimePresenter
 *
 * @package namespace App\Presenters;
 */
class TimePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TimeTransformer();
    }
}
