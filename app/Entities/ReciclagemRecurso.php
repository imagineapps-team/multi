<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ReciclagemRecurso.
 *
 * @package namespace App\Entities;
 */
class ReciclagemRecurso extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    public $logname    = "ReciclagemRecurso";
    
    protected $fillable = [
        'reciclagem_id',
        'recurso_id',
        'quantidade',
        'bonus_valor',
        'reciclador_id',
        'vigencia_id'
    ];

    public function reciclagem(){
        return $this->belongsTo(Reciclagem::class, 'reciclagem_id');
    }

    public function recurso(){
        return $this->belongsTo(Recurso::class, 'recurso_id');
    }

}
