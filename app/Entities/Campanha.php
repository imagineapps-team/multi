<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Campanha.
 *
 * @package namespace App\Entities;
 */
class Campanha extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'status',
        'data_inicial',
        'data_final',
        'modulo_id'
    ];

    public function modulo(){
        return $this->belongsTo(Modulo::class, 'modulo_id');
    }

    public function participantes(){
        return $this->belongsToMany(Contrato::class, 'contrato_campanhas');
    }

}
