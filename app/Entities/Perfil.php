<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;


class Perfil extends Model
{
    protected $table = 'perfis';
    public $timestamps = false;

    protected $fillable = [
        'nome', 'status', 'sigla'
    ];

}