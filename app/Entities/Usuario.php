<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Usuario extends Authenticatable
{
    use Notifiable, HasApiTokens;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $hasher;
    protected $table = 'usuarios';
    public $logname = 'Usuario';

    protected $fillable = [
        'id',
        'login',
        'nome',
        'senha',
        'remember_token',
        'status',
        'perfil_id',
        'pessoa_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'senha',
        'remember_token',
    ];
    
    public function perfil()
    {
	    return $this->belongsTo(Perfil::class);
    }

    public function pessoa()
    {
	    return $this->belongsTo(Pessoa::class);
    }

    public function findForPassport($identifier)
    {
        return $this->orWhere('login', $identifier)->first();
    }

    public function validateForPassportPasswordGrant($passpharse)
    {   
        return (hash('sha256', $passpharse) === $this->senha);
    }

    public function OauthAcessToken()
    {    
        return $this->hasMany('App\Entities\OauthAccessToken', 'user_id', 'id');
    }
    /*

    public function filhos() {
        return $this->belongsTo(Usuario::class, 'owner_usuario_id', 'id');
    }

    public function filhosCount()    {
        $count =  $this->filhos()->selectRaw('owner_usuario_id, count(*) as total')->groupBy('owner_usuario_id');
        return is_object($count->first()) ? $count->first()->total : 0;
    }
    */
}
