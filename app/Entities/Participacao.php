<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Participacao.
 *
 * @package namespace App\Entities;
 */
class Participacao extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $table   = 'clientes_eventos_contratos';
    public    $logname = 'Participacao';
    public    $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'cliente_id',
        'evento_id',
        'contrato_id',
        'modulo_id'
    ];

    public function cliente()
    {   
        return $this->belongsTo(Cliente::class, 'cliente_id');
    }

    public function evento()
    {   
        return $this->belongsTo(Evento::class, 'evento_id');        
    }

    public function contrato()
    {   
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }

    public function modulo()
    {   
        return $this->belongsTo(Modulo::class, 'modulo_id');
    }

}
