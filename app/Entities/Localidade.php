<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Localidade.
 *
 * @package namespace App\Entities;
 */
class Localidade extends Model implements Transformable
{
    use TransformableTrait;
    protected $table = 'comunidades';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nome', 'latitude','longintude'];

}
