<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UF extends Model
{

    public $timestamps = false;
    public $logname    = 'UF';
    protected $table   = "ufs";

    protected $fillable = [
        'nome', 'sigla'
    ];

    public function cidades(){
        return $this->hasMany(Cidade::class, 'uf_id');
    }

}
