<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Contrato.
 *
 * @package namespace App\Entities;
 */
class Contrato extends Model implements Transformable
{
    use TransformableTrait;
    /**
     * Removendo create_at e update_at
     */
    public $timestamps = false;
    public $logname = 'Contrato';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'numero',
        'numeroMedidor',
        'tensao',
        'complementoEndereco',
        'referenciaEndereco',
        'endereco',
        'numeroEndereco',
        'bairroEndereco',
        'cidadeEndereco',
        'endereco_uf_id',
        'nomeTitular',
        'cpf_cnpj_titular',
        'cep',
        'numeroParceiro',
        'bairro_id',
        'observacoes',
        'status',
        'contrato_tipo_id',
        'local_cadastro_id',
        'cliente_id',
        'data_cadastro'
    ];

    public function cliente()
    {   
        return $this->belongsTo(Cliente::class, 'cliente_id');
    }

}
