<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
/**
 * Class Log.
 *  
 * @package namespace App\Entities;
 */
class Log extends Model
{
    protected $table = "n_log";
    /**
     * Removendo create_at e update_at
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "nome_log",
        "model",
        "usuario_id",
        "registro_id",
        "tipo",
        "descricao",
        "data_cadastro",
        "ip"
    ];

}
