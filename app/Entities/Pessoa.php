<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Pessoa.
 *
 * @package namespace App\Entities;
 */
class Pessoa extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'status',
        'email',
        'sexo',
        'dt_nascimento',
        'imagem',
        'cod_aux',
        'obs',
        'endereco_id'
    ];

    public function usuario()
    {
		return $this->hasOne(Usuario::class);
	}

}
