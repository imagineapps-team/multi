<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;


class Menu extends Model
{
    protected $table	   = 'menus'; 
    protected $fillable  = [
      'status',
      'nome', 
      'controller', 
      'action',  
      'icone', 
      'descricao', 
      'ordem',
      'query',
      'menu_pai_id'
    ];

    public function modulos() {
	    return $this->belongsToMany('App\Entities\Modulo', 'menus_modulos', 'menu_id', 'modulo_id');
    }
    
    public function perfis() {
	    return $this->belongsToMany('App\Entities\Perfil', 'menus_perfils', 'menu_id', 'perfil_id');
    }
    
    
    public function isFather(){
	    return is_null($this->attributes['menu_pai_id']);
    }
    
    
    public function menu() {
	    return $this->hasMany(Menu::class, 'menu_pai_id', 'id');    
    }
    
}
