<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Vigencia.
 *
 * @package namespace App\Entities;
 */
class Vigencia extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'data_inicial',
        'data_final',
        'valor',
        'registro_id',
        'status',
        'model'
    ];

}
