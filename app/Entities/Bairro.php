<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Bairro extends Model
{
    public $logname    = 'Bairro';
    protected $table   = "bairros";

    protected $fillable = [
        'nome', 'cidade_id'
    ];

    public function cidade(){
        return $this->belongsTo(Cidade::class, 'cidade_id');
    }

    public function pessoas(){
        return $this->hasMany(Pessoa::class, 'bairro_id');
    }

    public function pessoasCount()    {
        $count =  $this->pessoas()->selectRaw('bairro_id, count(*) as total')->groupBy('bairro_id');
        return is_object($count->first()) ? $count->first()->total : 0;
    }
}
