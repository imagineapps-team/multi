<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    protected $fillable = [
      'nome', 'status' ,'icone', 'sigla'
    ];

    public function perfis() {
	    return $this->belongsToMany('App\Entities\Perfil', 'perfil_menus', 'modulo_id', 'perfil_id');
    }
    
    public function menus() {
	    return $this->belongsToMany('App\Entities\Menu', 'modulo_menus', 'modulo_id', 'menu_id');    
    }
}
