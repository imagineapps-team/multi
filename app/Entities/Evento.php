<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Evento.
 *
 * @package namespace App\Entities;
 */
class Evento extends Model implements Transformable
{
    use TransformableTrait;
    
    public $timestamps = false;
    public $logname = 'Evento';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'data_inical', 
        'data_final',
        'data_cadastro',
        'status'
    ];

    public function acao(){
        return $this->belongsTo(Acao::class, 'acao_id');
    }
    
    public function localidade(){
        return $this->belongsTo(Localidade::class, 'local_acao');
    }
    
}
