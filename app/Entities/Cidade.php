<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    public $timestamps = false;
    public $logname    = 'Cidade';
    protected $table   = "cidades";

    protected $fillable = [
        'nome', 'ibge_cod', 'uf_id'
    ];

    public function uf(){
        return $this->belongsTo(UF::class, 'uf_id');
    }

    public function bairros(){
        return $this->hasMany(Bairro::class, 'cidade_id');
    }
}
