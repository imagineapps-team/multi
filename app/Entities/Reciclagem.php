<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Reciclagem.
 *
 * @package namespace App\Entities;
 */
class Reciclagem extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    public $logname = 'Reciclagem';
    
    protected $fillable = [
        'codigo',
        'status',
        'cliente_evento_contrato_id',
        'bonus_total',
        'bonus_percentual',
        'data_cadastro'
    ];

    public function participacao(){
        return $this->belongsTo(Participacao::class, 'cliente_evento_contrato_id');
    }

    public function residuos(){
        return $this->hasMany(ReciclagemRecurso::class, 'reciclagem_id');
    }

}
