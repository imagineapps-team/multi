<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Cliente.
 *
 * @package namespace App\Entities;
 */
class Cliente extends Model implements Transformable
{
    use TransformableTrait;
    /**
     * Removendo create_at e update_at
     */
    public $timestamps = false;
    public $logname = 'Cliente';

    protected $fillable = [
        'nome',
        'cpf',
        'nis',
        'dataNascimento',
        'email',
        'celular',
        'como_conheceu',
        'data_cadastro',
        'evento_id'
    ];
}
