<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class FindUserByPerilCriteria.
 *
 * @package namespace App\Criteria;
 */
class FindUsuarioByPerilCriteria implements CriteriaInterface
{
    protected $request;
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function __construct(Request $request){
        $this->request = $request;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(['perfil_id' => $this->request->get('perfil_id')]);
    }
}
