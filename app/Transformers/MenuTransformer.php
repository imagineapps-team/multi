<?php

namespace App\Transformers;

use App\Entities\Menu;
use App\Http\Controllers\MenuController;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;

class MenuTransformer extends TransformerAbstract
{
    private $modulo;
    
    public function __construct($modulo) {
        $this->modulo = $modulo;
    }
    
    public function transform(Menu $menu)
    {
        $MenuController = new MenuController();
        
        $r['id']          = $menu->id;
        $r['controller']  = $menu->controller;
        $r['action']      = $menu->action;
        $r['nome']        = $menu->nome;
        $r['imagem']      = $menu->imagem;

        $submenus = $MenuController->getMenusByModuloId($menu->menu, $this->modulo);

        foreach ($submenus as $ch => $m){
            $r['submenus'][$ch] = self::transform($m);
        }
        
        return $r;
    }
}
