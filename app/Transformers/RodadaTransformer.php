<?php

namespace App\Transformers;

use App\Entities\Rodada;
use App\Repositories\RodadaRepository;
use League\Fractal\TransformerAbstract;

class RodadaTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Rodada $rodada)
    {
        $data_inicial = new \DateTime($rodada->periodo_inicial);
        $data_final     = new \DateTime($rodada->periodo_final);
        $data_aposta_inicial = new \DateTime($rodada->periodo_aposta_inicial);
        $data_aposta_final = new \DateTime($rodada->periodo_aposta_final);

        $transformer = [
            'id'         => $rodada->id,
            'titulo'     => $rodada->titulo,
            'pais_nome'  => $rodada->pais_nome,
            'pais_sigla' => $rodada->pais_sigla,
            'numero'     => $rodada->numero,
            'periodo_inicial' => $data_inicial->format('d/m/Y \a\s H:i:s'),
            'periodo_final' => $data_final->format('d/m/Y \a\s H:i:s'),
            'periodo_aposta_inicial' => $data_aposta_inicial->format('d/m/Y \a\s H:i:s'),
            'periodo_aposta_final' => $data_aposta_final->format('d/m/Y \a\s H:i:s')
        ];
        
        return $transformer; 
    }
}
