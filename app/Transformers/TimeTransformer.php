<?php

namespace App\Transformers;

use App\Entities\Time;
use App\Repositories\TimeRepository;
use League\Fractal\TransformerAbstract;

class TimeTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Time $time)
    {
        $transformer = [
            'id'              => $time->id,
            'pais'            => $time->pais,
            'nome'            => $time->nome,
            'sigla'           => $time->sigla,
            'logo'            => $time->logo,
            'status'          => $time->status
        ];
        
        return $transformer; 
    }
}
