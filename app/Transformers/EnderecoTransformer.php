<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Endereco;

/**
 * Class EnderecoTransformer.
 *
 * @package namespace App\Transformers;
 */
class EnderecoTransformer extends TransformerAbstract
{
    /**
     * Transform the Endereco entity.
     *
     * @param \App\Entities\Endereco $model
     *
     * @return array
     */
    public function transform(Endereco $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
