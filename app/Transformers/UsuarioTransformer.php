<?php

namespace App\Transformers;

use App\Entities\Usuario;
use App\Repositories\UsuarioRepository;
use League\Fractal\TransformerAbstract;

class UsuarioTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Usuario $user)
    {
	    $data_criacao = new \DateTime($user->created_at);

        $transformer = [
            'id'              => $user->id,
            'login'           => $user->login,
            'nome'            => $user->nome,
            'telefone_ddd'    => $user->telefone_ddd,
            'telefone_numero' => $user->telefone_numero,
            'cpf'             => $user->cpf,
            'dt_cadastro'     => $data_criacao->format('d/m/Y'),
            'perfil_id'       => $user->perfil_id,
            'perfil_nome'     => $user->perfil->nome,
            'perfil'          => $user->perfil,
            'status'          => $user->status
        ];

        if($user->perfil_id === 3) $transformer['cambistas'] = $user->cambistasCount();
        if($user->perfil_id === 2) $transformer['gerente']   = $user->gerente->nome;

        return $transformer; 
    }
}
