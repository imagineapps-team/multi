<?php

namespace App\Console\Commands\Pessoa;

use App\Entities\Pessoa;
use Illuminate\Console\Command;

class NewPessoa extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pessoa:inserir  {nome : Nome da Pessoa} 
                                            {--sexo= : Sexo com 1 caracter}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inserir nova pessoa';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $fields = [
                'nome' => $this->argument('nome'),
                'sexo' => strtoupper($this->option('sexo'))
            ];

            Pessoa::create($fields);

            echo "Pessoa ".$fields['nome']." inserida com sucesso";
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
    }
}
