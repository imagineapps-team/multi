<?php

namespace App\Console\Commands\Menu;

use App\Entities\Menu;
use App\Repositories\ModuloRepository;
use App\Repositories\PerfilRepository;
use App\Entities\Perfil;
use Illuminate\Console\Command;

class NewMenu extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'menu:inserir {nome : Nome de Exibição do Menu} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inserir um Novo Menu e atribuir o(s) Perfil(is) e o(s) Módulo(s) de acesso';

    protected $moduloRepository;
    protected $perfilRepository;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */


    public function __construct(ModuloRepository $moduloRepository,
                                PerfilRepository $perfilRepository){
        parent::__construct();

        $this->moduloRepository = $moduloRepository;
        $this->perfilRepository = $perfilRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = $this->getDataByQuestions();
        /*
        $data = [
            'nome'       => $this->argument('nome'),
            'status'     => 'A',
            'controller' => $controllerByQuestion,
            'action'     => $this->option('action'),
            'icone'      => $this->option('icone'),
            'descricao'  => $this->option('descricao'),
            'ordem'      => $this->option('ordem'),
        ];
        */

        dd($data);

        $modulos = $this->option('modulo');
        $perfis  = $this->option('perfil');

        dd($data);
    }

    private function getDataByQuestions(){
        $data = [];
        $data['controller'] = $this->ask('Nome da Controller (Rota Principal) Invocada pelo Menu ("null" para nenhuma)');
        $data['action']     = $this->ask('Nome da Ação/Método/Função (sub-rota) da Controller  ("null" para nenhuma)');
        
        $data['query']      = null;
        if($this->confirm('O menu possui alguma query após URL ? (ex.: cadastro?tipo=N) ')){
            $data['query'] = $this->ask('Informe query completa com a "?" ');
        }

        $data['descricao']  = $this->ask('Informe uma descrição para esse menu');
        $data['ordem']      = $this->ask('Ordem de exibição para esse menu em seu bloco (Digite o número da ordem ou "U" último)');
        
        $data['icone']      = null;
        if($this->confirm('Possui Ícone ?')){
            $data['icone'] = $this->ask('Nome do Ícone no Materialize');
        }

        $data['menu_pai_id'] = null;
        if($this->confirm('Este é um SUB-MENU?')){
            $data['menu_pai_id'] = $this->ask('Informe o ID do Menu PAI');
        }

        $data['modulos'] = $this->setModulos();
        $data['perfis']  = $this->setPerfis();
        
        return $data;
    }

    private function setModulos(&$modulosSelected = false){
        $list = $this->moduloRepository->list();
        if($modulosSelected){
            $list = array_diff($list, $modulosSelected);
        }  
        $modulosSelected[] = $this->choice('Qual Módulo esse Menu integra? ', $list);
        
        if($this->confirm('Possui outro?')){
            $this->setModulos($modulosSelected);
        }

        return $modulosSelected;
    }

    private function setPerfis(&$perfisSelected = false){
        $list = $this->perfilRepository->list();
        if($perfisSelected){
            $list = array_diff($list, $perfisSelected);
        }  
        $perfisSelected[] = $this->choice('Qual Perfil de acesso? ', $list);
        
        if($this->confirm('Possui outro?')){
            $this->setPerfis($perfisSelected);
        }

        return $perfisSelected;
    }
}
