<?php

namespace App\Console\Commands\Modulo;

use App\Entities\Modulo;
use Illuminate\Console\Command;

class NewModulo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'modulo:inserir {nome : Nome do Modulo} 
                                           {--icone= : Ícone no Padrão Material Icons}
                                           {--sigla= : Sigla do Modulo com 3 letras}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inserir um novo Módulo no sistema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $fields = [ 
                'nome'=>$this->argument('nome'),
                'icone'=>$this->option('icone'),
                'sigla'=>$this->option('sigla'),
                'status'=>'A' 
            ];
            
            Modulo::create($fields);

            echo 'Módulo '.$fields['nome'].' criado com sucesso';
        }catch(\Throwable $e){
            echo 'ERROR: '.$e->getMessage();
        }
        
    }
}
