<?php

namespace App\Console\Commands\Usuario;

use App\Entities\Pessoa;
use App\Entities\Perfil;
use App\Entities\Usuario;
use Illuminate\Console\Command;

class NewUsuario extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'usuario:inserir  {login : Login do usuario} 
                                             {--senha= : Senha do usuario}
                                             {--perfil= : Sigla do Perfil}
                                             {--nome=? : Nome do Usuário (Pessoa->nome)}
                                             {--sexo=? : Sexo do Usuário (Pessoa->sexo)}
                                             {--pessoa_id=? : ID da Pessoa (caso a pessoa já exista)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inserir nova pessoa';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $nomePessoa = $this->option('nome');
            if($nomePessoa !== '?'){
                $fieldsPessoa = [
                    'nome' => $nomePessoa,
                    'sexo' => $this->option('sexo')
                ];

                $pessoa = Pessoa::create($fieldsPessoa);
                $pessoa_id = $pessoa->id;
            }else{
                $pessoa_id = $this->option('pessoa_id');
            }

            $perfil = Perfil::where(['sigla' => $this->option('perfil')])->first();

            $fields = [
                'login'     => $this->argument('login'),
                'status'    => 'A',
                'senha'     => hash('sha256', $this->option('senha')),
                'remember_token' => hash('sha256', time()),
                'perfil_id' => $perfil->id,
                'pessoa_id' => $pessoa_id
            ];

            Usuario::create($fields);

            echo "Usuário ".$fields['login']." criado com sucesso";
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
    }
}
