<?php 

class AdminAuthenticated {
    public function handle($request, Closure $next)
    {
        
        if (Auth::user()) {
            return $next($request);
        }

        return redirect()->route('home'); // If user is not an admin.
    }
}