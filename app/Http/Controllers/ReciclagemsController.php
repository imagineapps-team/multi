<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ReciclagemCreateRequest;
use App\Http\Requests\ReciclagemUpdateRequest;
use App\Repositories\ReciclagemRepository;
use App\Validators\ReciclagemValidator;

use App\Entities\ReciclagemRecurso;


/**
 * Class ReciclagemsController.
 *
 * @package namespace App\Http\Controllers;
 */
class ReciclagemsController extends Controller
{
    /**
     * @var ReciclagemRepository
     */
    protected $repository;

    /**
     * @var ReciclagemValidator
     */
    protected $validator;

    /**
     * ReciclagemsController constructor.
     *
     * @param ReciclagemRepository $repository
     * @param ReciclagemValidator $validator
     */
    public function __construct(ReciclagemRepository $repository, ReciclagemValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $reciclagems = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $reciclagems,
            ]);
        }

        return view('reciclagems.index', compact('reciclagems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ReciclagemCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ReciclagemCreateRequest $request)
    {
        $timeini = microtime(true);
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $data = $request->all();
            
            $data['codigo'] = date('Y').strtoupper(str_replace(0, 9, substr(bin2hex(random_bytes(4)), 1))); //gerar auto alphanumerico
            $data['status'] = 'R';
            $data['data_cadastro'] = date('Y-m-d H:i:s');

            $reciclagem = $this->repository->create($data);
            
            foreach($data['residuos'] as &$residuo){
                $residuo = new ReciclagemRecurso($residuo);
            }

            $reciclagem->residuos()->saveMany($data['residuos']);
            
            $response = [
                'message' => 'Reciclagem criada',
                'data'    => $reciclagem->toArray(),
            ];
            
            \Log::info('SUCCESS: '. __METHOD__ .'. Tempo de Execução: '.(microtime(true) - $timeini).' milisegundos');

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            \Log::error('ERROR: '. __METHOD__ .'. Tempo de Execução: '.microtime(true) - $timeini.' milisegundos');
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reciclagem                             = $this->repository->find($id);
        $reciclagem['participacao']             = $reciclagem->participacao;
        $reciclagem['participacao']['contrato'] = $reciclagem->participacao->contrato;
        $reciclagem['participacao']['cliente']  = $reciclagem->participacao->cliente;
        $reciclagem['residuos']                 = $reciclagem->residuos;

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $reciclagem,
            ]);
        }

        return view('reciclagems.show', compact('reciclagem'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reciclagem = $this->repository->find($id);

        return view('reciclagems.edit', compact('reciclagem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ReciclagemUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ReciclagemUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $reciclagem = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Reciclagem updated.',
                'data'    => $reciclagem->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Reciclagem deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Reciclagem deleted.');
    }


    public function getByParticipacaoId($id){
        $reciclagem = $this->repository->findWhere(['cliente_evento_contrato_id' => $id]);
        
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $reciclagem->all(),
            ]);
        }

        return view('reciclagems.getByParticipacaoId', compact('reciclagem'));
    }
}
