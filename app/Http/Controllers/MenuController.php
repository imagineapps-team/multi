<?php

namespace App\Http\Controllers;


use App\Entities\Menu;
use App\Entities\Modulo;
use App\Transformers\MenuTransformer;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection; 
use Illuminate\Http\Request;


class MenuController extends Controller
{
    /**
     * Retorna todos menus de acesso do usuário de acordo com o Módulo passado pelo paramento
     *
     * @param int $idModulo \App\Entities\Modulo {id}
     * @return \Spatie\Fractal\Fractal
     */
    public function getMenusModuloByLogin($idModulo)
    {        
        $user   = Auth::guard('api')->user();   

        $menus  = self::getMenusByModuloId($user->perfil->menus, $idModulo);

        return fractal($menus->where('menu_pai_id', null), new MenuTransformer($idModulo));
    }

    /**
     * @param Collection $menusByLogin
     * @param $idModulo
     * @return \Illuminate\Support\Collection
     */
    public function getMenusByModuloId(Collection $menusByLogin, $idModulo)
    {
        $r            = collect();
        $modulo       = Modulo::findOrFail($idModulo);
        $modulo_menus = $modulo->menus()->orderBy('nome', 'asc')->get();

        foreach($modulo_menus as $menu){
            $m = $menusByLogin->where('id', $menu->id)->first();
            if($m !== null){
                if($m->id == $menu->id) $r->push($menu);
            }
            unset($m);
        }
        return $r;
    }
}
