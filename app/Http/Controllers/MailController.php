<?php

namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use App\Mail\ReciboMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

use App\Repositories\ReciclagemRepository;
 
class MailController extends Controller
{
    protected $reciclagemRepository;

    public function __construct(ReciclagemRepository $reciclagemRepository){
        $this->reciclagemRepository = $reciclagemRepository;
    }

    public function send(Request $request, $layout)
    {
        switch($layout){
            case "recibo":
                $this->sendMailReciboReciclagem($request->all());
                break;
        }
    }

    private function sendMailReciboReciclagem($data){

        $emailTo   = $data['emailTo'];

        $reciclagem = $this->reciclagemRepository->find($data["reciclagem_id"]);
        $data_hora = new \DateTime($reciclagem->data_cadastro);
        $reciclagem['participacao']             = $reciclagem->participacao;
        $reciclagem['participacao']['contrato'] = $reciclagem->participacao->contrato;
        $reciclagem['participacao']['cliente']  = $reciclagem->participacao->cliente;
        $reciclagem['data_hora']                = $data_hora->format('d/m/Y \à\s H:i'); 

        foreach($reciclagem->residuos as &$residuo){
            $residuo['recurso'] = $residuo->recurso;
        }
        $reciclagem['residuos'] = $reciclagem->residuos;

        try{
            if(Mail::to($emailTo)->send(new ReciboMail($reciclagem))){
                return response()->json(['success' => 'E-mail enviado'], 200);
            }else{
                return response()->json(['error' => 'Não foi possível enviar o'], 500);
            }
            
        }catch(\Throwable $e){
            return response()->json(['error' => $e->getMessage()], 500);
        }
        
    }


}