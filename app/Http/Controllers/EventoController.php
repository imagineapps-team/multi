<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\EventoCreateRequest;
use App\Http\Requests\EventoUpdateRequest;
use App\Repositories\EventoRepository;
//use App\Validators\EventoValidator;

/**
 * Class EventosController.
 *
 * @package namespace App\Http\Controllers;
 */
class EventoController extends Controller
{
    /**
     * @var EventoRepository
     */
    protected $repository;

    /**
     * @var EventoValidator
     */
  //  protected $validator;

    /**
     * EventosController constructor.
     *
     * @param EventoRepository $repository
     * @param EventoValidator $validator
     */
    public function __construct(EventoRepository $repository/*, EventoValidator $validator*/)
    {
        $this->repository = $repository;
        //$this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $eventos = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $eventos,
            ]);
        }

        return view('eventos.index', compact('eventos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EventoCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(EventoCreateRequest $request)
    {
        try {

          //  $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $evento = $this->repository->create($request->all());

            $response = [
                'message' => 'Evento created.',
                'data'    => $evento->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $evento = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $evento,
            ]);
        }

        return view('eventos.show', compact('evento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $evento = $this->repository->find($id);

        return view('eventos.edit', compact('evento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EventoUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(EventoUpdateRequest $request, $id)
    {
        try {
            
            //$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $evento = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Evento editado',
                'data'    => $evento->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Evento deleted.', 
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Evento deleted.');
    }

    /**/
    public function getEventosDisponiveis(){
        $eventos = $this->repository->findWhereIn('status',['C', 'F']);

        foreach($eventos as &$evento){
            $evento['acao'] = $evento->acao;
            $evento['localidade'] = $evento->localidade;
        }
        
        return response()->json([
           'data' => $eventos,
        ]);        
    }

}
