<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ParticipacaoCreateRequest;
use App\Http\Requests\ParticipacaoUpdateRequest;
use App\Repositories\ParticipacaoRepository;
use App\Validators\ParticipacaoValidator;

/**
 * Class ParticipacaoController.
 *
 * @package namespace App\Http\Controllers;
 */
class ParticipacaoController extends Controller
{
    /**
     * @var ParticipacaoRepository
     */
    protected $repository;

    /**
     * @var ParticipacaoValidator
     */
    protected $validator;

    /**
     * ParticipacaosController constructor.
     *
     * @param ParticipacaoRepository $repository
     * @param ParticipacaoValidator $validator
     */
    public function __construct(ParticipacaoRepository $repository, ParticipacaoValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $participacoes = $this->repository->all()->take(20);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $participacoes,
            ]);
        }

        return view('participacaos.index', compact('participacaos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ParticipacaoCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ParticipacaoCreateRequest $request)
    {
        $timeini = microtime(true);
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            
            $participacao = $this->repository->create($request->all());

            $response = [
                'message' => 'Participacão adicionada',
                'data'    => $participacao->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            \Log::info('SUCCESS: '. __METHOD__ .'. Tempo de Execução: '.(microtime(true) - $timeini).' milisegundos');
            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            \Log::error('ERROR: '. __METHOD__ .'. Tempo de Execução: '.(microtime(true) - $timeini).' milisegundos');
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }catch (\Throwable $ex) {
            $participacao = $this->repository->findWhere($request->all()) [0];
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => $ex->getMessage(),
                    'participacao' => $participacao->toArray() 
                ], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $participacao = $this->repository->find($id);

        $participacao['cliente']  = $participacao->cliente;
        $participacao['evento']   = $participacao->evento;
        $participacao['contrato'] = $participacao->contrato;

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $participacao,
            ]);
        }

        return view('participacaos.show', compact('participacao'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $participacao = $this->repository->find($id);

        return view('participacaos.edit', compact('participacao'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ParticipacaoUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ParticipacaoUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $participacao = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Participacao updated.',
                'data'    => $participacao->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->repository->delete($id);
            return response()->json(['success'=>'Participação excluída'], 200);
        }catch (ModelNotFoundException $ex) {
            return response()->json(['error'=> $ex->getMessage()], 404);
        } catch (\Throwable $ex) {
            return response()->json(['error'=> $ex->getMessage()], 500);
        }

        /*
        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Participacao excluída.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Participacao excluída.');
        */
    }

    public function getPartipacoesByEvento($evento_id){
        
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $participacoes = $this->repository->findWhere(['evento_id' => $evento_id]);
        
        foreach($participacoes as &$participacao){
            $participacao['cliente'] = $participacao->cliente;
            $participacao['evento'] = $participacao->evento;
            $participacao['contrato'] = $participacao->contrato;
        }

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $participacoes,
            ]);
        }

        return view('participacaos.index', compact('participacaos'));
    }

}
