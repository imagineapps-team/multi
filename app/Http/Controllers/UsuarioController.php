<?php
namespace App\Http\Controllers;

use App\Entities\Usuario;
use App\Entities\Perfil;
use App\Http\Requests\UsuarioCreateRequest;
use App\Http\Requests\UsuarioUpdateRequest;
use App\Repositories\UsuarioRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsuarioController extends Controller
{
    /**
     * @var UsuarioRepository
     */
    private $repository;

    /**
     * UsuarioController constructor.
     * @param UsuarioRepository $repository
     */
    public function __construct(UsuarioRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Retornando o usuário autenticado
     *
     * @return mixed
     */
    public function getUser()
    {
        $user = Auth::guard('api')->user();

        return response()->json([
            'user' => $this->repository->find($user->id)
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));    
            $this->repository->pushCriteria(app('App\Criteria\FindUsuarioByPerilCriteria'));

            $users = $this->repository->paginate(10);
            //return response()->json($users);

            if(request()->wantsJson()){    
                return response()->json($users);    
            }

            switch($request->get('perfil_id')){
                case 1: 
                    $titulo = 'Independentes';
                    $fields = [ 'Nome'    =>'nome', 
                                'Login'   =>'login', 
                                'Telefone'=>['telefone_ddd', 'telefone_numero'], 
                                'Perfil'  =>'perfil_nome',
                                'CPF'     => 'cpf', 
                                'Status'  =>'status'];             
                    break;
                case 2:
                    $titulo = 'Cambistas';  
                    $fields = [ 'Nome'    =>'nome', 
                                'Login'   =>'login', 
                                'Telefone'=>['telefone_ddd', 'telefone_numero'], 
                                'Saldo'   =>'status',
                                'Gerente' => 'gerente', 
                                'Status'  =>'status'];
                    break;
                case 3: 
                    $titulo = 'Gerentes'; 
                    $fields = [ 'Nome'    =>'nome', 
                                'Login'   =>'login', 
                                'Telefone'=> ['telefone_ddd', 'telefone_numero'], 
                                'Cambistas'=>'cambistas', 
                                'Status'  =>'status'];
                    break;
                default:

                    $titulo = 'Todos'; 
                    $fields = [ 'Nome'    =>'nome', 
                                'Login'   =>'login', 
                                'Telefone'=> ['telefone_ddd', 'telefone_numero'], 
                                'CPF'     =>'cpf', 
                                'Status'  =>'status'];

                    $this->repository->popCriteria(app('App\Criteria\FindUsuarioByPerilCriteria'));
                    $users = $this->repository->paginate(10);                                
                    break;                    
            
            } 
            
            return view('usuarios.index')->with(['usuarios' => $users, 'titulo'=>$titulo, 'fields'=>$fields]);
        } catch(\Throwable $e){
            return response()->json(['erro' => $e->getMessage()], 422);
        }
        
    }

    public function create(){
        $perfis   = Perfil::orderBy('nome', 'asc')->pluck('nome', 'id')->prepend('Escolha o Perfil', 0);
        
        return view('admin.usuarios.form')->with(['perfis'=>$perfis]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  UsuarioCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UsuarioCreateRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['remember_token'] = str_random(10);
            $attributes['gerente_usuario_id'] = ($attributes['gerente_usuario_id'] == 'null') ? null : $attributes['gerente_usuario_id'];

            if (isset($attributes['senha']) && $attributes['senha']) {
                $attributes['senha'] = hash('sha256',$attributes['senha']);
            }

            $user   = Usuario::create($attributes);

            return response()->json($this->repository->parserResult($user));

        } catch (\Throwable $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = Usuario::find($id);

            if(request()->wantsJson()){
                return response()->json($user);
            }

            $perfis   = Perfil::orderBy('nome', 'asc')->pluck('nome', 'id')->prepend('Escolha o Perfil', 0);
            $gerentes = Usuario::where(['perfil_id' =>3])->pluck('nome', 'id')->prepend('Escolha o gerente', 'null'); 

            return view('usuarios.form')->with(['usuario' => $user, 'perfis' => $perfis, 'gerentes' => $gerentes]);

        } catch (ModelNotFoundException $e) {
            return response(['error' => 'Usuário não encontrado'], 404);
        } catch (\Throwable $e) {
            return response(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UsuarioUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(UsuarioUpdateRequest $request, $id)
    {
        
        try {

            $data = $request->all();
            $data['gerente_usuario_id'] = ($data['gerente_usuario_id'] == 'null') ? null : $data['gerente_usuario_id'];

            $save = Usuario::find($id)->update($data);

            if($save){
                $user = Usuario::find($id);
            }
            // O repository não está retornando A entidade quando o Request vem... não sei pq
            
            /* $this->repository->skipPresenter(true);
            $userOld = $this->repository->find($id);

            $user = $this->repository->update($data, $id);
            $this->repository->skipPresenter(false);
            */
            return response()->json($user->toArray());
        } catch (\Throwable $e) {
            dd($e);
            die();
            return response(['error' => $e->__toString()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {      
        try {
            /**
             * Entender pq o Repository nao funciona na deleção (?!?!?!?)
             */
            $user = Usuario::find($id);
            $deleted = $user->delete($id);

            if (!$deleted) {
                throw new \Exception("Usuário não pode ser removido");
            }

            return response()->json(['Usuário removido'], 204);
        } catch (\Throwable $e) {
            return response(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPermissoes($id)
    {
        try {
            return response()->json($this->repository->getPermissoesById($id));
        } catch (\Throwable $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPermissionsGroupByApplications($id)
    {
        try {
            $aplicacoes = $this->repository->getPermissionsGroupByApplications($id);
            $permissoes = $this->repository->getPermissoesById($id);

            return response()->json([
                'aplicacoes' => $aplicacoes,
                'permissoes' => array_values($permissoes)
            ]);
        } catch (\Throwable $e) {
            return response()->json(['error' => $e->getLine()], 500);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function attachPermissions(Request $request, $id)
    {
        try {
            $this->repository->skipPresenter(true);
            $usuario = $this->repository->find($id);
            $this->repository->skipPresenter(false);

            $idPermissao = $request->get('idPermissao');
            $acao = $request->get('acao');
            $objPermissao = $this->permissaoRepository->find($idPermissao);

            if ($acao == 'add') {
                if (!$usuario->menu()->find($objPermissao->menu_id)) {
                    $usuario->menu()->attach($objPermissao->menu_id);
                }
                $usuario->permissoes()->attach($objPermissao->id);
            }

            if ($acao == 'delete') {
                if ($objPermissao->menu_id) {
                    $usuario->menu()->detach($objPermissao->menu_id);
                }

                $usuario->permissoes()->detach($objPermissao->id);
            }

            $permissoes = $this->repository->getPermissoesById($usuario->id);

            return response()->json($permissoes);
        } catch (\Throwable $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function check(Request $request)
    {
        try {
            $resultList = [];
            $checked = false;

            $token = $request->get('token');

            $resultQuery = \DB::table('usuarios')
                ->where('token', $token)
                ->select('users.id', 'users.status')
                ->first();

            if ($resultQuery && $resultQuery->status != 'A') {
                $checked = true;
                $user = Usuario::find($resultQuery->id);
                $user->status = 'A';
                $pessoa = $user->pessoa;
                $user->save();

                \Mail::send('emails.dados-acesso', ['user' => $user], function ($message) use ($pessoa) {
                    $message->from(env('MAIL_FROM_ADDRESS'))
                        ->to($pessoa->email)
                        ->subject('Sisatos - Dados de acesso');
                });
            }

            $resultList = [
                'checked' => $checked
            ];

            return view('auth.ativacao-cadastro', compact('resultList'));
        } catch (\Throwable $e) { 
            return view('auth.ativacao-cadastro', ['checked' => false]);
        }
    }

    public function login(){
        if(Auth::guard('admin')->check()){
            return view('admin.dashboard');
        }else{
            return view('admin.auth.login');
        }
    }


    public function logout()
    { 
        if (Auth::check()) {
            if(Auth::guard('admin')) Auth::guard('admin')->logout();
            if(Auth::guard('api')->check()) Auth::guard()->user()->OauthAcessToken()->delete();

            if (request()->wantsJson()) {
                return response()->json(['logout' => 'success'], 200);
            }
            
            return view('admin.auth.login');
        }
    }

    public function saveUserByToken(){
        $user_oauth = Auth::guard('api')->user();
        $user       = Usuario::where(['id' => $user_oauth->id])->first();

        
        try{
            Auth::guard('admin')->login($user);
            return response()->json(['user' => $user]);
        }catch(\Throwable $e){
            return response()->json(['error' => $e->getMessage()], 500);   
        }
    }
}
