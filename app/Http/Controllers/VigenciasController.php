<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\VigenciaCreateRequest;
use App\Http\Requests\VigenciaUpdateRequest;
use App\Repositories\VigenciaRepository;
use App\Validators\VigenciaValidator;

/**
 * Class VigenciasController.
 *
 * @package namespace App\Http\Controllers;
 */
class VigenciasController extends Controller
{
    /**
     * @var VigenciaRepository
     */
    protected $repository;

    /**
     * @var VigenciaValidator
     */
    protected $validator;

    /**
     * VigenciasController constructor.
     *
     * @param VigenciaRepository $repository
     * @param VigenciaValidator $validator
     */
    public function __construct(VigenciaRepository $repository, VigenciaValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $vigencias = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $vigencias,
            ]);
        }

        return view('vigencias.index', compact('vigencias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  VigenciaCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(VigenciaCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $vigencium = $this->repository->create($request->all());

            $response = [
                'message' => 'Vigencia created.',
                'data'    => $vigencium->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vigencium = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $vigencium,
            ]);
        }

        return view('vigencias.show', compact('vigencium'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vigencium = $this->repository->find($id);

        return view('vigencias.edit', compact('vigencium'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  VigenciaUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(VigenciaUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $vigencium = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Vigencia updated.',
                'data'    => $vigencium->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Vigencia deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Vigencia deleted.');
    }

    /** Custom */

    public function searchResiduosByEvento($evento_id){
        try{
            $residuos = DB::table('evento_reciclador_vigencias')
            ->join('eventos',   'evento_reciclador_vigencias.evento_id',   '=', 'eventos.id')
            ->join('vigencias', 'evento_reciclador_vigencias.vigencia_id', '=', 'vigencias.id')
            ->join('recursos', function ($join) {
                $join->on('vigencias.registro_id', '=', 'recursos.id')
                     ->where('vigencias.model', '=', 'Recurso');
            })
            ->join('unidade_medidas', 'recursos.unidade_medida_id', '=', 'unidade_medidas.id')
            ->select('vigencias.valor', 
                     'recursos.nome', 
                     'recursos.id AS recurso_id', 
                     'unidade_medidas.sigla AS medidoem',
                     'evento_reciclador_vigencias.vigencia_id as vigencia_id',
                     'evento_reciclador_vigencias.reciclador_id as reciclador_id')
            ->where(['eventos.id' => $evento_id, 'evento_reciclador_vigencias.status' => 'A'])
            ->get();
            
            if (request()->wantsJson()) {

                return response()->json([
                    'residuos' => $residuos->toArray()
                ]);
            }
        }catch (ModelNotFoundException $ex) {
            return response()->json(['error'=> $ex->getMessage()], 404);
        }catch (\Throwable $ex) {
            return response()->json(['error'=> $ex->getMessage()], 500);
        }


    }
}
