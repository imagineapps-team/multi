<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\RecursoCreateRequest;
use App\Http\Requests\RecursoUpdateRequest;
use App\Repositories\RecursoRepository;
use App\Validators\RecursoValidator;

/**
 * Class RecursosController.
 *
 * @package namespace App\Http\Controllers;
 */
class RecursosController extends Controller
{
    /**
     * @var RecursoRepository
     */
    protected $repository;

    /**
     * @var RecursoValidator
     */
    protected $validator;

    /**
     * RecursosController constructor.
     *
     * @param RecursoRepository $repository
     * @param RecursoValidator $validator
     */
    public function __construct(RecursoRepository $repository, RecursoValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $recursos = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $recursos,
            ]);
        }

        return view('recursos.index', compact('recursos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RecursoCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(RecursoCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $recurso = $this->repository->create($request->all());

            $response = [
                'message' => 'Recurso created.',
                'data'    => $recurso->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recurso = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $recurso,
            ]);
        }

        return view('recursos.show', compact('recurso'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recurso = $this->repository->find($id);

        return view('recursos.edit', compact('recurso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RecursoUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(RecursoUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $recurso = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Recurso updated.',
                'data'    => $recurso->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Recurso deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Recurso deleted.');
    }
}
