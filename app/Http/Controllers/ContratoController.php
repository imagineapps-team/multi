<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Entities\Cliente;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ContratoCreateRequest;
use App\Http\Requests\ContratoUpdateRequest;
use App\Repositories\ContratoRepository;
use App\Repositories\ClienteRepository;

/**
 * Class ContratoController.
 *
 * @package namespace App\Http\Controllers;
 */
class ContratoController extends Controller
{
    /**
     * @var ContratoRepository
     */
    protected $repository;

     /**
     * @var ClienteRepository
     */
    private $clienteRepository;

    /**
     * ContratosController constructor.
     *
     * @param ContratoRepository $repository
     */
    public function __construct(ContratoRepository $repository,
                                ClienteRepository $clienteRepository
    )
    {
        $this->repository = $repository;
        $this->clienteRepository = $clienteRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $contratos = $this->repository->all()->take(20);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $contratos,
            ]);
        }

        return view('contratos.index', compact('contratos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ContratoCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ContratoCreateRequest $request)
    {
        $timeini = microtime(true);

        $data = $request->all();
        try {
            $contrato = $this->repository->create($request->all());
            if($data['cliente']) $this->setCliente($request, $contrato->id);
            
            $response = [
                'message' => 'Contrato criado',
                'data'    => $contrato->toArray(),
            ];
            
            \Log::info('SUCCESS: '. __METHOD__ .'. Tempo de Execução: '.(microtime(true) - $timeini).' milisegundos');
            if ($request->wantsJson()) {
                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            \Log::error('ERROR: '. __METHOD__ .'. Tempo de Execução: '.(microtime(true) - $timeini).' milisegundos');

            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contrato = $this->repository->find($id);
        if($contrato->cliente) $contratos['cliente'] = $contrato->cliente;

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $contrato,
            ]);
        }

        return view('contratos.show', compact('contrato'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contrato = $this->repository->find($id);

        return view('contratos.edit', compact('contrato'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ContratoUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ContratoUpdateRequest $request, $id)
    {
        try {
            //$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $contrato = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Contrato editado.',
                'data'    => $contrato->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Contrato deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Contrato deleted.');
    }
    /**
     * Metodo que pesquisa os contratos por Nome ou Numero do Contrato (NUC)
     */
    public function searchByNomeOrNumero($searchTxt){
        $key = (is_numeric($searchTxt)) ? 'numero' : 'nomeTitular';
        
        $contratos = $this->repository->findWhere([
            [$key, 'LIKE', '%'.$searchTxt.'%']
        ])->take(20);

        if (request()->wantsJson()) {

            return response()->json([
                'contratos' => $contratos,
            ]);
        }

        return view('contratos.show', compact('contrato'));
    }

    /** */

    private function setCliente($request, $id, $return = null){
        $data    = $request->all();
        try {
            $this->clienteRepository->skipPresenter(true);

            $contrato = $this->repository->find($id);

            if (is_integer($contrato->cliente_id)) {
                $this->clienteRepository->update($data['cliente'], $contrato->cliente_id);
            } else {
                $cliente = $this->clienteRepository->create($data['cliente']);
                $contrato->cliente_id = $cliente->id;
                $contrato->save();
            }

            if(is_null($return)) return;

            $this->repository->skipPresenter(false);
            $this->clienteRepository->skipPresenter(false);

            return response()->json($this->repository->parserResult($cliente));
        } catch (ModelNotFoundException $ex) {
            return response()->json([], 404);
        }  catch (\Throwable $ex) {
            return response()->json(['error'=> $ex->getMessage()], 500);
        }
    }
}
