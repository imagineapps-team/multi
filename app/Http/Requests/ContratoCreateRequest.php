<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContratoCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //email|unique:pessoas,email|max:255
            //'numero' => 'numero|unique:contratos,numero|max:50',
            'nomeTitular' => 'required|min:5',
            'status' => 'required|max:1',
            'cep' => 'required'
        ];
    }
}
