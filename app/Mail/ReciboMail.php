<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReciboMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $reciclagem;

    public function __construct($reciclagem)
    {
        $this->reciclagem = $reciclagem;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('lightreciclaop@gmail.com', 'LightRecicla')
                    ->subject('LIGHTRECICLA | CÓD: '.$this->reciclagem->codigo)
                    ->view('emails.reciboreciclagem');
    }


}
