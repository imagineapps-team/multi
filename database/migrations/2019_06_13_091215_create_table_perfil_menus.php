<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePerfilMenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfil_menus', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('perfil_id')->default();
            $table->unsignedInteger('menu_id')->default();
            
            $table->unique(['perfil_id', 'menu_id']);
            
            $table->foreign('perfil_id')->references('id')->on('perfis');
            $table->foreign('menu_id')->references('id')->on('menus');
	    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfil_menus');
    }
}
