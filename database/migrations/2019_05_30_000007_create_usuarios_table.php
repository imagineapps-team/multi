<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Usuarios.
 */
class CreateUsuariosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuarios', function(Blueprint $table) {
            $table->increments('id');
			
			$table->string('login', 50);
			$table->string('senha', 90);
			$table->string('remember_token', 150);
			$table->string('status', 1);
			
			$table->unsignedInteger('perfil_id')->nullable();
			$table->foreign('perfil_id')->references('id')->on('perfis');
			
			$table->unsignedInteger('pessoa_id')->nullable();
			$table->foreign('pessoa_id')->references('id')->on('pessoas');
			
			$table->unique('login');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuarios');
	}
}
