<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('n_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model');
            $table->dateTime('data_cadastro');
            $table->integer('usuario_id')->unsigned();
            $table->text('descricao');
            $table->integer('registro_id');
            $table->char('tipo', 1); //  I = INSERT, U = UPDATE, D = DELETE
            $table->char('nome_log', 50);
            $table->string('ip', 16);
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('n_log');
    }
}
