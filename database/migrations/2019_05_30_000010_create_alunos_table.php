<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAlunosTable.
 */
class CreateAlunosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alunos', function(Blueprint $table) {
            $table->increments('id');
			$table->string('status', 1)->default('A');
			$table->string('codigo')->nullable();
			$table->boolean('destaque')->nullable();
			$table->boolean('primeiro_emprego')->nullable();
			$table->boolean('cadastro_completo')->nullable();
			$table->boolean('sem_experiencia')->nullable();
			$table->string('como_conheceu', 3)->nullable();

			$table->unsignedInteger('pessoa_id')->nullable();
			$table->foreign('pessoa_id')->references('id')->on('pessoas');
        
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('alunos');
	}
}
