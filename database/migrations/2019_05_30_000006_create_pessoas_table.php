<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePessoasTable.
 */
class CreatePessoasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pessoas', function(Blueprint $table) {
			$table->increments('id');
			$table->string('status', 1)->default('A');
			$table->string('nome');
			$table->string('sexo');
			$table->date('dt_nascimento')->nullable();
			$table->string('email', 150)->nullable();
			$table->string('imagem', 90)->nullable();
			$table->string('obs')->nullable();

			$table->unsignedInteger('endereco_id')->nullable();
            $table->foreign('endereco_id')->references('id')->on('enderecos');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pessoas');
	}
}
