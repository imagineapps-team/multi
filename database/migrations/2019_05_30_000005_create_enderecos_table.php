<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateEnderecosTable.
 */
class CreateEnderecosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('enderecos', function(Blueprint $table) {
            $table->increments('id');
			$table->string('cep', 8);
			$table->string('logradouro');
			$table->string('numero', 10);
			$table->string('tipo', 1)->default('R'); //C = COMERCIAL; R = RESIDENCIAL
			
			$table->unsignedInteger('bairro_id');
            $table->foreign('bairro_id')->references('id')->on('bairros');
			
			$table->string('complemento')->nullable();
			$table->string('latidude')->nullable();
			$table->string('longitude')->nullable();
			
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('enderecos');
	}
}
