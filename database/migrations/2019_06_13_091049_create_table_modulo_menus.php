<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModuloMenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modulo_menus', function (Blueprint $table) {
            $table->increments('id');
	    
            $table->unsignedInteger('modulo_id')->default();
            $table->unsignedInteger('menu_id')->default();
            
            $table->unique(['modulo_id', 'menu_id']);
            
            $table->foreign('modulo_id')->references('id')->on('modulos');
            $table->foreign('menu_id')->references('id')->on('menus');
	    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modulo_menus');
    }
}
