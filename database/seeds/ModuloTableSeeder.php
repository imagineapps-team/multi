<?php

use App\Entities\Modulo;
use Illuminate\Database\Seeder;

class ModuloTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo 'Criando Módulos do sistema...';
        foreach($this->modulos() as $sigla=>$nome){
                factory(Modulo::class)->create([
                    'nome'  => $nome,
                    'sigla' => $sigla,
                    'status'=> 'A', 
                ]);    
        }
        echo '[OK] Módulos criados! ';
    }

    private function modulos(){
        return [
            'EQP'=>'DOAÇÃO DE EQUIPAMENTOS',
            'LAM'=>'DOAÇÃO DE LÂMPADAS',
            'REC'=>'RECICLAGEM',
            'EDU'=>'EDUCATIVO'
        ];
    }
}
