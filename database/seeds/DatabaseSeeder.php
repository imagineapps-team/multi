<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ModuloTableSeeder::class);
        $this->call(PerfilTableSeeder::class);
        $this->call(UfTableSeeder::class);
        $this->call(CidadeTableSeeder::class);
    }
}
