<?php

use Illuminate\Database\Seeder;
use App\Entities\Perfil;

class PerfilTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo 'Criando perfis existentes no sistema...';
        foreach($this->niveis() as $sigla=>$nome){
                factory(Perfil::class)->create([
                    'nome'  => $nome,
                    'sigla' => $sigla,
                    'status'=> 'A', 
                ]);    
        }
        echo '[OK] Perfis criados! ';
    }

    private function niveis(){
        return [
            'ADM'=>'ADMINISTRADOR',
            'CC'=>'CONCESSIONÁRIA',
            'PMS'=>'PROMOTOR SOCIAL'
        ];
    }
}
