<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$factory->define(App\Entities\Perfil::class, function(){ return []; });
$factory->define(App\Entities\UF::class, function(){ return []; });
$factory->define(App\Entities\Cidade::class, function(){ return []; });
$factory->define(App\Entities\Modulo::class, function(){ return []; });
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Entities\Usuario::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'nome' => $faker->name,
        'senha' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
